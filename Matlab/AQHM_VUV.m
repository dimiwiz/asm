function P = AQHM_VUV(speechFile, gender)
% Speech/nonspeech and voiced/unvoiced regions estimation.
%
%  ----INPUT PARAMETERS----
%    speechFile: speech wav file
%    gender: gender of speaker (male/female)
%
%  ----SAVED PARAMETERS----
%    P.ti: analysis time instants
%    P.isS: is speech
%    P.isV: is voiced
%
%  (c) Yannis Pantazis, 2013
%  mail: kafentz@csd.uoc.gr
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[s, fs] = audioread(speechFile);

disp(size(s));
disp(fs);

Fc = 30;
[bHigh, aHigh] = ellip(6, .5, -60, 2*Fc/fs, 'high');
s = filtfilt(bHigh, aHigh, s);

len = length(s);

% some thresholds
s_ns_thres = -60; % (in dB) speech/nonspeech threshold   (lower values ==> larger speech segments)
v_uv_thres = 10;  % (in dB) voiced/unvoiced threshold   (larger values ==> larger voiced segments)
s_sm_thres = -50; % (in dB) smoothed speech threshold   (lower values ==> larger voiced segments)

s = filtfilt(bHigh, aHigh, s);
% some parameters are gender sensetive
if strcmp(gender,'male')
    Fc = 1000; % cutoff frequency
elseif strcmp(gender,'female')
    Fc = 1500;
end

[bLow, aLow] = ellip(6, .5, 60, 2*Fc/fs, 'low');
s_smooth = filtfilt(bLow, aLow, s);


winLen = round(30*10^(-3)*fs); % 30ms
if mod(winLen,2) == 0
    winLen = winLen + 1;
end
step = round(5*10^(-3)*fs); % 5ms
N = (winLen-1)/2; % half of analysis window
n = -N:N;

ti = 1:step:len;
No_ti = length(ti);
isS = zeros(No_ti,1);
isV = zeros(No_ti,1);


% estimation of speech/nonspeech and voiced/unvoiced
for i = 1:No_ti
    if ti(i)>N && ti(i)<len-N
        spEn = 20*log10(std(s(ti(i)+n)));
        spEn_sm = 20*log10(std(s_smooth(ti(i)+n)));
        
        isS(i) = spEn > s_ns_thres;
        if isS(i)
            isV(i) = (spEn-spEn_sm < v_uv_thres) && (spEn_sm > s_sm_thres);
        end
    end
end

% perform smoothing using median filter
medOrd = 5;
isS = medfilt(isS, medOrd);
isV = medfilt(isV, medOrd);

% plot(s);hold on;plot(s_smooth,'r');
% plot(ti, 0.4*isS, 'g*');plot(ti, 0.4*isV, 'kd');

% save the parameters
P = [];
for i = 1:No_ti
    P(i).ti = ti(i);
    P(i).isS = isS(i);
    P(i).isV = isV(i);
end
t = 0:1/fs:length(s)/fs - 1/fs;
figure; plot(t,s); hold on; plot(ti/fs, isV); hold off;
figure; plot(t,s); hold on; plot(ti/fs, isS, 'k'); hold off;
save([speechFile '_VU.mat'], 'P');

