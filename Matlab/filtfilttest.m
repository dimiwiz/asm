% Filter coefficients
B = [1, -1];
A = [1, -0.5, 0.5];
% Random input
 %x = 1000*randn(1,1000);
x = audioread('C:\Users\dimiwiz\Desktop\SA19.WAV');

% MATLAB's filtfilt
y = filtfilt(B1, A1, x);

% My filtfilt
y1 = filter(B, A, x);
y2 = filter(B, 1, fliplr(y1));
out = fliplr(y2);

% Plots
subplot(211);
plot(y,'r'); hold on; plot(out, 'g'); 
hold off; grid;
title('Two ways to zero-phase filter');
legend('filtfilt', 'my 2-way filter');
xlabel('Time (samples)');
subplot(212);
plot(abs(y-out));
grid; title('Absolute Error');
xlabel('Time (samples)');