function y = medfilt(x, p)
% Performs median filtering of order p.
%
%  ----INPUT PARAMETERS----
%    x: signal or sequence
%    p: order of the filter
%
%  ----OUTPUT PARAMENTERS----
%    y: filtered signal or sequence
%
% (c) Yannis Pantazis, 2007, csd, uoc
%     mail: pantazis@csd.uoc.gr

x = x(:)';
L = length(x);

ad = (p-1)/2; 
if ad == 0
    y = x;
    return;
end

x = [x(1)*ones(1,ad), x, x(L)*ones(1,ad)];
A = fliplr(toeplitz(fliplr(x(1:L)),x(L:L+p-1))');
y = median(A);