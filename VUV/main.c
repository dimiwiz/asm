#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "wave.h"
#include "audio.h"
#include "math_funcs.h"

 char *filename;
 struct audio audio;
 int i,j;
 
 double test[] = {1,2,3,4,5,7,32,6,1,3}; //medfilt test
 
 //filtfilt test
 
 double b[] = {0.9367,-5.6201,14.0502,-18.7336,14.0502,-5.6201,0.9367};
 double a[] = {1.0000,-5.9843,14.9216,-19.8435,14.8438,-5.9221,0.9844};
 double x[] = {0.8147,0.9058,0.1270,0.9649,0.9134,0.9706,0.6324,0.0975,0.9575,0.9649,0.1576,0.9706,0.9572,0.4854,0.8003,0.1419,0.4218,0.9157,0.7922,0.9595};
 double *y;

//ellip test
double *aH;
double *bH;

int main(int argc, char **argv) {

    filename = (char*) malloc(sizeof(char) * 1024);
    if (filename == NULL) {
        printf("Error in malloc\n");
        exit(1);
    }

     // get file path
     /*char cwd[1024];
     if (getcwd(cwd, sizeof(cwd)) != NULL) {
       
        strcpy(filename, cwd);

        // get filename from command line
        if (argc < 2) {
          printf("No wave file specified\n");
          return;
        }
        
        strcat(filename, "/");
        strcat(filename, argv[1]);
        printf("%s\n", filename);
     }*/

    //audio = WavRead("C:\\Users\\dimiwiz\\Desktop\\SA19.wav");

    /* Medfilt test code
    int l = sizeof(test)/sizeof(double);

    y = medfilt(test,5,l); 
     
    for(i=0; i < l; i++){
        printf("to median tou y einai: %lf \n", y[i]);
    }
      
     /till here */
    
    /* filtfilt code till next comments 
    
    int la = sizeof(a)/sizeof(double);
    int lb = sizeof(b)/sizeof(double);
    int lx = sizeof(x)/sizeof(double);
    
    y = malloc(sizeof(double)*lx);
    
    y = filtfilt(b,a,x,lb,la,lx);
    for( i = 0; i < lx; i++){
        printf("%lf  ",y[i]);
    }
    
     till here*/
     // ellip test code
     aH = malloc(sizeof(double)*7);
     bH = malloc(sizeof(double)*7);
     
     double wp = (double)(2*30)/(double)16000;
     printf("%lf  \n",wp);
     ellip(6,0.5,-60.00,wp,3,16000,aH,bH);
     for( i = 0; i < 7; i++){
         printf("%lf  %lf\n",aH[i],bH[i]);
     }
    //till here 
    
    // cleanup before quitting
    free(filename);
    return 0;

}
