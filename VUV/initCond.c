#include "zi.h"

struct zi initCond(double *b, double *a, int npts, int *lb,int *la){
    struct zi z;
    double *temp,*vals,*rhs;
    int *rows,*cols;
    double **spr;
    int nfilt;
    int nfact;
    int i,j;
    
    if(*la > *lb){
        nfilt = *la;
    }
    else{
        nfilt = *lb;
    }
    
    nfact = (nfilt-1) * 3;
    
    if(npts <= nfact){
        //printf("error\n");
    }
    
    if(*lb < nfilt){
        temp = malloc(sizeof(double)*nfilt);
        temp = b;
        for(i = *lb; i < nfilt; i++){
           temp[i] = 0;
        }
        b = realloc(b,sizeof(double)*(nfilt-*lb));
        b = temp;
        *lb = nfilt;
        
    }
    else if(*la < nfilt){
        temp = malloc(sizeof(double)*nfilt);
        temp = a;
        for(i = *la; i < nfilt; i++){
            temp[i] = 0;
        }
        a = realloc(a,sizeof(double)*(nfilt-*la));
        a = temp;
        *la = nfilt;
    }
    
    for ( i = 0; i < nfilt; i++){
        //printf("exoume mesA: %lf\n",a[i]);
    }
    
    rows = malloc(sizeof(int)*(3*nfilt-5));
    
    for(i = 0; i < nfilt-1; i++){
        rows[i] = i+1;
    }
    for(i = 1; i < nfilt-1; i++){
        rows[i+nfilt-2] = i+1;
    }
    for(i = 0; i < nfilt-2; i++){
        rows[i+(2*nfilt-3)] = i+1;
    }
    
    for(i = 0; i < 3*nfilt-5; i++){
        printf("exoume mesa: %d\n",rows[i]);
    }
    printf("\n\n");
    
    cols = malloc(sizeof(int)*(3*nfilt-5));
    
    cols = ones(cols,nfilt-1,0);
    
    for(i = 1; i < nfilt-1; i++){
        cols[i+nfilt-2] = i+1;
    }
    for( i = 1; i < nfilt-1; i++){
        cols[i+(2*nfilt-4)] = i+1;
    }
    
    for(i = 0; i < 3*nfilt-5; i++){
        printf("exoume mesa: %d\n",cols[i]);
    }
    
    vals = malloc(sizeof(double)*(3*nfilt-5));

    vals[0] = a[1]+1;
    for( i = 0; i < nfilt-2; i++){
        vals[i+1] = a[i+2];
    }
    
    vals = d_ones(vals,nfilt-2,nfilt-1);
    vals = m_ones(vals,nfilt-2,2*nfilt-3);
    
    printf("\n\n");
    for(i = 0; i < 3*nfilt-5; i++){
        printf("exoume mesa: %lf\n",vals[i]);
    }
    
    rhs = malloc(sizeof(double)*nfilt-1);
    for(i = 0; i < nfilt-1; i++){
        rhs[i] = (b[i+1]-(b[0]*a[i+1]));
    }
    
    printf("\n\n");
    for(i = 0; i < nfilt-1; i++){
        printf("exoume mesa: %lf\n",rhs[i]);
    }

    spr = sparse(rows,cols,vals,3*nfilt-5);
    
    int size_r = maxEl(rows,3*nfilt-5);
    int size_c = maxEl(cols,3*nfilt-5);
    printf("\n\n");
    for(i = 0; i < size_r; i++){
        printf("\n");
        for( j = 0 ; j < size_c; j++){
            printf("%lf   ",spr[i][j]);
        }
    }
    printf("\n\n");
    
    LUdecomp(spr,size_r,rhs);
    
    
    printf("\n\n");
    
    z.zi_ar = rhs;
    z.l = size_r;
    
    return z;
    
    }