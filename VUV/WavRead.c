#include "audio.h"
#include "wave.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

FILE *ptr;
struct HEADER header;
struct audio af;

unsigned char buffer4[4];
unsigned char buffer2[2];
long offset;

struct audio WavRead(char *fn){
     // open file
 printf("Opening  file..\n");
 ptr = fopen(fn, "rb");
 if (ptr == NULL) {
	printf("Error opening file\n");
	exit(1);
 }
 
 
 
 
 int read = 0;
 
 // read header parts
offset = ftell(ptr);
printf("pointer se thesi %ld \n", offset);

 read = fread(header.riff, sizeof(header.riff), 1, ptr);
 printf("(1-4): %s \n", header.riff); 

fseek(ptr, 16, SEEK_CUR);

 //Format type
 read = fread(buffer2, sizeof(buffer2), 1, ptr); 
 header.format_type = buffer2[0] | (buffer2[1] << 8);
 char format_name[10] = "";
 
 if (header.format_type == 1)
   strcpy(format_name,"PCM"); 
 else if (header.format_type == 6)
  strcpy(format_name, "A-law");
 else if (header.format_type == 7)
  strcpy(format_name, "Mu-law");

//Channels
 read = fread(buffer2, sizeof(buffer2), 1, ptr);
 header.channels = buffer2[0] | (buffer2[1] << 8);
 printf("(23-24) Channels: %u \n", header.channels);




offset = ftell(ptr);
printf("pointer se thesi %ld \n", offset);

//Sample rate
 read = fread(buffer4, sizeof(buffer4), 1, ptr);
 header.sample_rate = buffer4[0] |
						(buffer4[1] << 8) |
						(buffer4[2] << 16) |
						(buffer4[3] << 24);

 printf("(25-28) Sample rate: %u\n", header.sample_rate);

fseek(ptr, 6, SEEK_CUR);

//Check if here start the data of our file
 read = fread(buffer2, sizeof(buffer2), 1, ptr);
 header.bits_per_sample = buffer2[0] |
					(buffer2[1] << 8);


 read = fread(header.data_chunk_header, sizeof(header.data_chunk_header), 1, ptr);
 printf("(37-40) Data Marker: %s \n", header.data_chunk_header);

 offset = ftell(ptr);
printf("pointer se thesi %ld \n", offset);

 read = fread(buffer4, sizeof(buffer4), 1, ptr);
 header.data_size = buffer4[0] |
				(buffer4[1] << 8) |
				(buffer4[2] << 16) | 
				(buffer4[3] << 24 );

 // calculate no.of samples
 
 offset = ftell(ptr);
printf("pointer se thesi %ld \n", offset);

 long num_samples = (8 * header.data_size) / (header.channels * header.bits_per_sample);
 printf("Number of samples:%lu \n", num_samples);

 long size_of_each_sample = (header.channels * header.bits_per_sample) / 8;
 printf("Size of each sample:%ld bytes\n", size_of_each_sample);
 
 af.s = (int*)malloc(sizeof(int)*num_samples);
 
 if (header.format_type == 1) { // PCM
    printf("Store sample data? Y/N?");
	char c = 'n';
	scanf("%c", &c);
	if (c == 'Y' || c == 'y') { 
		long i =0;
		char data_buffer[size_of_each_sample];
		int  size_is_correct = 1;

        long bytes_in_each_channel = (size_of_each_sample / header.channels);
            if ((bytes_in_each_channel  * header.channels) != size_of_each_sample) {
			printf("Error: %ld x %ud <> %ld\n", bytes_in_each_channel, header.channels, size_of_each_sample);
			size_is_correct = 0;
		}

 
		if (size_is_correct) { 
					// the valid amplitude range for values based on the bits per sample
			long low_limit = 0l;
			long high_limit = 0l;

			switch (header.bits_per_sample) {
				case 8:
					low_limit = -128;
					high_limit = 127;
					break;
				case 16:
					low_limit = -32768;
					high_limit = 32767;
					break;
				case 32:
					low_limit = -2147483648;
					high_limit = 2147483647;
					break;
			}					

			printf("\n\n.Valid range for data values : %ld to %ld \n", low_limit, high_limit);
			for (i =1; i <= num_samples; i++) {
				//printf("==========Sample %ld / %ld=============\n", i, num_samples);
				read = fread(data_buffer, sizeof(data_buffer), 1, ptr);
				if (read == 1) {
				
					// dump the data read
					unsigned int  xchannels = 0;
					int data_in_channel = 0;

					for (xchannels = 0; xchannels < header.channels; xchannels ++ ) {
						//printf("Channel#%d : ", (xchannels+1));
						// convert data from little endian to big endian based on bytes in each channel sample
						if (bytes_in_each_channel == 4) {
							data_in_channel =	data_buffer[0] | 
												(data_buffer[1]<<8) | 
												(data_buffer[2]<<16) | 
												(data_buffer[3]<<24);
						}
						else if (bytes_in_each_channel == 2) {
							data_in_channel = data_buffer[0] & 255 |
												(data_buffer[1] << 8);
						}
						else if (bytes_in_each_channel == 1) {
							data_in_channel = data_buffer[0];
						}

						//printf("%d ", data_in_channel);
                        
                        af.s[i] = data_in_channel;

						// check if value was in range
						if (data_in_channel < low_limit || data_in_channel > high_limit)
							printf("**value out of range\n");

						//printf(" | ");
					}

					//printf("\n");
				}
				else {
					printf("Error reading file. %d bytes\n", read);
					break;
				}

			} // 	for (i =1; i <= num_samples; i++) {

		} // 	if (size_is_correct) { 

	 } // if (c == 'Y' || c == 'y') { 
 }
 
 af.fs = header.sample_rate;

 printf("Closing file..\n");
 fclose(ptr);
 
 return af;
}