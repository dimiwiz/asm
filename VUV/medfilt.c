#include <stdio.h>
#include <stdlib.h>
#include "math_funcs.h"

double *medfilt(double *x, int p, int l){
    double *y;
    double *tempx,*tempx1,*tempx2;
    double **res,**res_tr;
    
    int i,j;
    
    int ad = (p-1)/2;
    
    if(ad == 0){
        y = x;
        return y;
    }
    printf("to size einai: %d\n",l); //GAITI TO THELW GIA NA MH GINEI ALLAGI STO NOUMER?
    tempx = (double*)malloc(sizeof(double)*(ad+l+ad));
    
    for(i = 0; i < ad; i++){
        tempx[i] = x[0] * 1;
    }
    
    for( i = 0; i < l; i++){
        tempx[i+ad] = x[i];
    }
    
    for(i = 0; i < ad; i++){
        tempx[i+ad+l] = x[l-1] * 1;
    }
    
    for(i = 0; i < ad+l+ad; i++){
       //printf(" mpika %lf \n", tempx[i]);
    }
    
    
    tempx1 = fliplr(tempx,l);
    
    /*for(i = 0; i < l; i++){
        printf("sto x1 exw: %lf\n",tempx1[i]);
    }*/
    
    tempx2 = (double*)malloc(sizeof(double)*(p));
    
    for(i = 0; i < p; i++){
        tempx2[i] = tempx[(l-1)+i];
    }
    
    /*for(i = 0; i < p; i++){
        printf("sto x2 exw: %lf\n",tempx2[i]);
    }*/
    
    res = toeplitz(tempx1,tempx2,l,p);
    /*for(i = 0; i < l; i++){
        for( j = 0 ; j < p; j++){
            printf("%lf  ",res[i][j]);
        }
        printf("\n");
        printf("\n");
    }
    
    printf("\n");
    printf("\n");
    */
    
    res_tr = transpose(res,l,p);
    /*for(i = 0; i < p; i++){
        for( j = 0 ; j < l; j++){
            printf("%lf  ",res_tr[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    printf("\n");
    */
    res_tr = flip2d(p,l,res_tr);
    
    /*printf("\n");
    printf("\n");
    for(i = 0; i < p; i++){
        for( j = 0 ; j < l; j++){
            printf("%lf  ",res_tr[i][j]);
        }
        printf("\n");
    }
    
    //printf("\n");
    //printf("\n");
    */
    y = median(p,l,res_tr);
    
    return y;
}
