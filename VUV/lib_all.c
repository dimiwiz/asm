/* Library functions for the GCA Speech Modification part
 *
 * Written by: George Kafentzis
 * 	       Stagiaire,
 * 	       France Telecom R&D, Site du Lannion, 
 * 	       2008
 *
 * e-mail: kafentz@csd.uoc.gr
 * 	   gkafen@gmail.gr
 */
#include "all.h"

#define TINY 1.0e-20
#define EPS 2.0e-6
#define MAXM 100
#define EPSS 1.0e-7
#define MR 8
#define MT 10
#define MAXIT (MT*MR)
#define MAXM_2 50
#define RADIX 2.0

#define  N_a    16807                              /* that is, \(a = 7\sp{5}\) */
#define  N_m    2147483647                         /* that is, \(m = 2\sp{31}-1\) */
#define  N_q    127773                             /* note, \(q = m/a\) = quotient */
#define  N_r    2836                               /* note, \(r = m\%a\) = remainder */



#define SWAP(a,b) { temp = (a); (a) = (b); (b) = temp; }
#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))

//#define DEBUG
//#define POLY2LSF_DEBUG
//#define SM_DEBUG




// TO DO:
// ------
// 1) Argument checking in the last few functions
//
//





//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//												
//												
//	    			G E N E R A L    F U N C T I O N S				
//												
//												
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////






/* Function : wait
 *
 * Purpose : pauses program execution for seconds
 *
 * Input Arguments : int seconds <- the seconds the program will pause
 *
 * Output Arguments: -
 *
 */
void wait(int seconds)
{
	clock_t endwait;
	endwait = clock () + seconds * CLOCKS_PER_SEC ;
  	
	while (clock() < endwait) 
  	{
		;
  	}
}





/* Function : w2f
 *
 * Purpose : prints a variable to a file
 *
 * Input Arguments : int frameNo    <- the frame number it is printed, -1 if name is needed ("frame" is default name)
 * 		     char *name     <- the name of the output file
 * 		     void *variable <- the variable to be printed
 * 		     int N          <- the length of the variable
 * 		     char c         <- the data type of the variable ('C' -> complex, 'd' -> double, 'i' -> integer)
 *
 * Output Arguments: -
 *
 */
void w2f(int frameNo, char *name, void *variable, int N, char c)
{
	int ti;
	char str[6];
	FILE *fp = NULL;
	double *dptr = NULL;
	int *iptr = NULL;
	char *nptr = NULL;
	COMPLEX *cptr = NULL;
	char a[10] = { 'f','r','a','m','e','\0' };

	if (frameNo != -1)
	{
		my_itoa(frameNo, str);
		strcat(a, str);
		nptr = a;
	}
	else
		nptr = name;

	if ((fp = fopen(nptr, "w")) == NULL)
	{
		printf("Cannot open file %s!\n", nptr);
		exit(EXIT_FAILURE);
	}

	dptr = (double *)variable;
	iptr = (int *)variable;
	cptr = (COMPLEX *)variable;

	ti = 0;
	if (N > 1)
	{
		while(1)
		{
			if (c == 'd')
				fprintf(fp, "%.30lf\n", dptr[ti++]);
			else if (c == 'i')
				fprintf(fp, "%d\n", iptr[ti++]);
			else if (c == 'C')
			{
				fprintf(fp, "%.20lf + %.20lf*i\n", cptr[ti].real, cptr[ti].imag);
				ti++;
			}
			else
			{
				printf("Character not supported!\n");
				return;
			}

			if(ti == N)
				break;
		}
	}
	else if (N == 1)
	{
		if (c == 'd')
			fprintf(fp, "%.30g\n", *dptr);
		else if (c == 'i')
			fprintf(fp, "%d\n", *iptr);
		else if (c == 'C')
			fprintf(fp, "%.20lf + %.20lf*i\n", cptr[0].real, cptr[0].imag);
		else
		{
			printf("Character not supported!\n");
			return;
		}
	}
	else
		printf("Invalid length number!\n");

	fclose(fp);

	printf("File %s is successfully written on disk!\n", nptr);
}





/* Function : print
 *
 * Purpose : prints an array of size N (integer or double)
 *
 * Input Arguments : double * <- the input array
 *                   int N    <- the size of the input array
 *                   char c   <- 'i' or 'd', for integer or double array
 *
 * Output Arguments: -
 *
 */
void print(double *x, int N, char c)
{
	int i;
	double *dptr = x;
	int *iptr = (int *)x;

	if (c == 'd')
	{
		for (i = 0; i < N; i++)
			printf("%.30g\n", dptr[i]);
	}
	else if (c == 'i')
	{
		for (i = 0; i < N; i++)
			printf("%d\n", iptr[i]);
	}
	else
		printf("Character not supported!\n");
}




/* Function : reverse_str
 *
 * Purpose : reverses a string
 *
 * Input Arguments : char s[] <- the string to store the result
 *
 * Output Arguments: -
 *
 */
void reverse_str(char s[])
{
	int c, i, j;

    	for (i = 0, j = strlen(s)-1; i < j; i++, j--) 
    	{
        	c = s[i];
       		s[i] = s[j];
        	s[j] = c;
    	}
}



/* Function : my_itoa
 *
 * Purpose : converts an integer value to a string
 *
 * Input Arguments : int n    <- the integer number to be converted
 * 		     char s[] <- the string to store the result
 *
 * Output Arguments: -
 *
 */
void my_itoa(int n, char s[])
{
	int i, sign;

	if ((sign = n) < 0)  /* record sign */
		n = -n;          /* make n positive */
	i = 0;

	do 
	{       /* generate digits in reverse order */
		s[i++] = n % 10 + '0';   	/* get next digit */
	} while ((n /= 10) > 0);     	/* delete it */

	if (sign < 0)
		s[i++] = '-';

	s[i] = '\0';

	reverse_str(s);
}






//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//												//
//												//
//	    			S I M P L E   M A T H   F U N C T I O N S			//
//												//
//												//
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////






/* Function : max_L
 *
 * Purpose : calculates the maximum value between two values
 *
 * Input Arguments : double x <- the first number
 * 		     double y <- the second number
 *
 * Output Arguments: double <- the maximum value
 *
 */
double max_L(double x, double y)
{
	if(x >= y)
		return x;
	else
		return y;
}






/* Function : min_L
 *
 * Purpose : calculates the minimum value between two values
 *
 * Input Arguments : double x <- the first number
 * 		     double y <- the second number
 *
 * Output Arguments: double <- the minimum value
 *
 */
double min_L(double x, double y)
{
	if(x <= y)
		return x;
	else
		return y;
}





/* Function : maxEl
 *
 * Purpose : calculates the maximum value of an input array
 *
 * Input Arguments : double *a <- the number array
 * 		     int N     <- the length of the array
 *
 * Output Arguments: double <- the maximum value of the input array
 *
 */
double maxEl(double *a, int N)
{
	int i;
	double max = a[0];

	if (N <= 0)
	{
		printf("maxEl() : N should be positive!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N; i++)
		if (max < a[i])
			max = a[i];
	return max;
}





/* Function : minEl
 *
 * Purpose : calculates the minimum value of an input array
 *
 * Input Arguments : double *a <- the number array
 * 		     int N     <- the length of the array
 *
 * Output Arguments: double <- the minimum value of the input array
 *
 */
double minEl(double *a, int N)
{
	int i;
	double min = a[0];

	if (N <= 0)
	{
		printf("minEl() : N should be positive!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N; i++)
		if (min > a[i])
			min = a[i];
	return min;
}




/* Function : meanVal
 *
 * Purpose : calculates the mean value of the input array
 *
 * Input Arguments : double *s <- the number array
 * 		     int N     <- the length of the array
 *
 * Output Arguments: double <- the mean value of the input array
 *
 */
double meanVal(double *s, int N)
{
	int i;
	double res = 0.0;

	if (s == NULL)
	{
		printf("meanVal() : Input argument is NULL!\n");
		exit(EXIT_FAILURE);
	}

	if (N <= 0)
	{
		printf("meanVal() : N should be positive!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N; i++)
		res += s[i];

	return res/(N);
}



/* Function : stdev
 *
 * Purpose : calculates the standard deviation of the input array
 *
 * Input Arguments : double *s <- the number array
 * 		     int N     <- the length of the array
 *
 * Output Arguments: double <- the standard deviation of the input array
 *
 */
double stdev(double *s, int N)
{
	int i;
	double res = 0.0;
	double M = meanVal(s, N);

	if (s == NULL)
	{
		printf("stdev() : Input argument is NULL!\n");
		exit(EXIT_FAILURE);
	}

	if (N <= 0)
	{
		printf("stdev() : N should be positive!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N; i++)
		res += (s[i] - M)*(s[i] - M);

	res = (double)res/(N - 1);

	return sqrt(res);
}







/* Function : sortC
 *
 * Purpose : sorts a complex array according to the real part of each complex element in ascending order
 *
 * Input Arguments : COMPLEX *array <- the array to be sorted according to its real part
 *                   int N          <- the size of the array
 *
 * Output Arguments: -
 */
void sortC(COMPLEX *array, int N)
{ 
	int i, j; 
	COMPLEX tmp;

	for (i = 0; i < N-1; i++) 
	{
		for (j = N-1; j > i; j--) 
		{
			if (array[j-1].real > array[j].real) 
			{
				tmp.real = array[j].real;
				tmp.imag = array[j].imag;
				array[j].real = array[j-1].real;
				array[j].imag = array[j-1].imag;
				array[j-1].real = tmp.real;
				array[j-1].imag = tmp.imag;
			} 
		}
	}
}





/* Function : sort
 *
 * Purpose : sorts a real array in ascending order
 *
 * Input Arguments : double *array <- the array to be sorted
 *                   int N         <- the size of the array
 *
 * Output Arguments: -
 */
void sort(double *array, int N)
{ 
	int i, j; 
	double tmp;

	for (i = 0; i < N-1; i++) 
	{
		for (j = N-1; j > i; j--) 
		{
			if (array[j-1] > array[j]) 
			{
				tmp = array[j];
				array[j] = array[j-1];
				array[j-1] = tmp;
			} 
		}
	}
}









/* Function : absVal
 *
 * Purpose : calculates the absolute value of the elements of an array
 *
 * Input Arguments : double *s <- the number array
 * 		     int N     <- the length of the array
 *
 * Output Arguments: double * <- the absolute value of the elements of the input array
 *
 */
double *absVal(double *s, int N)
{
	int i;
	double *res = calloc(N, sizeof(double));

	if (s == NULL)
	{
		printf("absVal() : Input argument is NULL!\n");
		exit(EXIT_FAILURE);
	}

	if (N <= 0)
	{
		printf("absVal() : N must be positive!\n");
		exit(EXIT_FAILURE);
	}

	if (res == NULL)
	{
		printf("absVal() : Could not allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N; i++)
		res[i] = fabs(s[i]);

	return res;
}








/* Function : CComplex
 *
 * Purpose : defines a complex number
 *
 * Input Arguments : double re <- the real part of the complex number
 * 		     double im <- the imaginary part of the complex number
 *
 * Output Arguments: COMPLEX <- the resulting complex number
 *
 * Taken from : Numerical Recipes in C, 1992.
 */
COMPLEX CComplex(double re, double im)
{
	COMPLEX c;

	c.real = re;
	c.imag = im;

	return c;
}








/* Function : Cadd
 *
 * Purpose : adds two complex numbers
 *
 * Input Arguments : COMPLEX a <- the first complex number
 * 		     COMPLEX b <- the second complex number
 *
 * Output Arguments: COMPLEX <- the result of the addition
 *
 * Taken from : Numerical Recipes in C, 1992.
 */
COMPLEX Cadd(COMPLEX a, COMPLEX b)
{
	COMPLEX c;

	c.real = a.real + b.real;
	c.imag = a.imag + b.imag;

	return c;
}








/* Function : Csub
 *
 * Purpose : subtracts two complex numbers
 *
 * Input Arguments : COMPLEX a <- the first complex number
 * 		     COMPLEX b <- the second complex number
 *
 * Output Arguments: COMPLEX <- the result of the subtraction
 *
 * Taken from : Numerical Recipes in C, 1992.
 */
COMPLEX Csub(COMPLEX a, COMPLEX b)
{
	COMPLEX c;

	c.real = a.real - b.real;
	c.imag = a.imag - b.imag;

	return c;
}









/* Function : Cmul
 *
 * Purpose : multiplies two complex numbers
 *
 * Input Arguments : COMPLEX a <- the first complex number
 * 		     COMPLEX b <- the second complex number
 *
 * Output Arguments: COMPLEX <- the result of the multiplication
 *
 * Taken from : Numerical Recipes in C, 1992.
 */
COMPLEX Cmul(COMPLEX a, COMPLEX b)
{
	COMPLEX c;

	c.real = a.real*b.real - a.imag*b.imag;
	c.imag = a.imag*b.real + a.real*b.imag;

	return c;
}








/* Function : Conjg
 *
 * Purpose : finds the conjugate of a complex number
 *
 * Input Arguments : COMPLEX z <- the complex number
 *
 * Output Arguments: COMPLEX <- the conjugate of the input
 *
 * Taken from : Numerical Recipes in C, 1992.
 */
COMPLEX Conjg(COMPLEX z)
{
	COMPLEX c;
	c.real = z.real;
	c.imag = -z.imag;

	return c;
}









/* Function : Cdiv
 *
 * Purpose : divides two complex numbers
 *
 * Input Arguments : COMPLEX a <- the first complex number
 * 		     COMPLEX b <- the second complex number
 *
 * Output Arguments: COMPLEX <- the result of the division
 *
 * Taken from : Numerical Recipes in C, 1992.
 */
COMPLEX Cdiv(COMPLEX a, COMPLEX b)
{
	COMPLEX c;
	double r, den;

	if (fabs(b.real) >= fabs(b.imag))
	{
		r = b.imag/b.real;
		den = b.real + r*b.imag;
		c.real = (a.real + r*a.imag)/den;
		c.imag = (a.imag - r*a.real)/den;
	}
	else 
	{
		r = b.real/b.imag;
		den = b.imag + r*b.real;
		c.real = (a.real * r + a.imag)/den;
		c.imag = (a.imag * r - a.real)/den;
	}

	return c;
}









/* Function : Csqrt
 *
 * Purpose : finds the square root of a complex number
 *
 * Input Arguments : COMPLEX z <- the complex number
 *
 * Output Arguments: COMPLEX <- the result of the operation
 *
 * Taken from : Numerical Recipes in C, 1992.
 */
COMPLEX Csqrt(COMPLEX z)
{
	COMPLEX c;
	double x, y, w, r;

	if ((z.real == 0.0) && (z.imag == 0.0))
	{
		c.real = 0.0;
		c.imag = 0.0;
		return c;
	}
	else
	{
		x = fabs(z.real);
		y = fabs(z.imag);

		if (x >= y)
		{
			r = y/x;
			w = sqrt(x) * sqrt(0.5 * (1.0 + sqrt(1.0 + r*r)));
		}
		else
		{
			r = x/y;
			w = sqrt(y) * sqrt(0.5 * (r + sqrt(1.0 + r*r)));
		}

		if (z.real >= 0.0)
		{
			c.real = w;
			c.imag = z.imag/(2.0*w);
		}
		else
		{
			c.imag = (z.imag >= 0) ? w : -w;
			c.real = z.imag/(2.0*c.imag);
		}

		return c;
	}
}










/* Function : Cabs
 *
 * Purpose : calculates the absolute value of a complex number
 *
 * Input Arguments : COMPLEX z <- the complex number
 *
 * Output Arguments: double <- the absolute value of the input
 *
 * Taken from : Numerical Recipes in C, 1992.
 */
double Cabs(COMPLEX z)
{
	double x, y, ans, temp;
	
	x = fabs(z.real);
	y = fabs(z.imag);

	if (x == 0.0)
		ans = y;
	else if (y == 0.0)
		ans = x;
	else if (x > y)
	{
		temp = y/x;
		ans = x*sqrt(1.0 + temp*temp);
	}
	else
	{
		temp = x/y;
		ans = y*sqrt(1.0 + temp*temp);
	}

	return ans;
}








/* Function : RCmul
 *
 * Purpose : multiplies a double with a complex number
 *
 * Input Arguments : double x  <- the real number (double)
 * 		     COMPLEX a <- the complex number
 *
 * Output Arguments: COMPLEX <- the result of the multiplication
 *
 * Taken from : Numerical Recipes in C, 1992.
 */
COMPLEX RCmul(double x, COMPLEX a)
{
	COMPLEX c;
	
	c.real = x*a.real;
	c.imag = x*a.imag;

	return c;
}








/* Function : CabsVal
 *
 * Purpose : calculates the absolute value of the elements of a complex array
 *
 * Input Arguments : COMPLEX *s <- the number array
 * 		     int N      <- the length of the array
 *
 * Output Arguments: double * <- the absolute value of the elements of the input array
 *
 */
double *CabsVal(COMPLEX *s, int N)
{
	int i;
	double t1 = 0.0, t2 = 0.0;
	double *res = calloc(N, sizeof(double));

	if (s == NULL)
	{
		printf("CabsVal() : Input argument is NULL!\n");
		exit(EXIT_FAILURE);
	}

	if (N <= 0)
	{
		printf("CabsVal() : N must be positive!\n");
		exit(EXIT_FAILURE);
	}

	if (res == NULL)
	{
		printf("CabsVal() : Could not allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N; i++)
	{
		//printf("%lf + %lf\n", s[i].real, s[i].imag);
		t1 = s[i].real * s[i].real;
		t2 = s[i].imag * s[i].imag;
		res[i] = sqrt(t1 + t2);
	}

	return res;
}








/* Function : angle
 *
 * Purpose : calculates the phase angles, in radians, of a vector with complex elements.  
 *
 * Input Arguments : COMPLEX *s <- the complex array
 * 		     int N      <- the array size
 *
 * Output Arguments: double * <- the angle of each vector complex element
 *
 */
double *angle(COMPLEX *s, int N)
{
	double *res = NULL;
	int i;

	if (s == NULL)
	{
		printf("angle() : the input argument is NULL!\n");
		exit(EXIT_FAILURE);
	}
	if (N <= 0)
	{
		printf("angle() : N must be positive!\n");
		exit(EXIT_FAILURE);
	}

	if ((res = calloc(N, sizeof(double))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N; i++)
		res[i] = atan2(s[i].imag, s[i].real);

	return res;
}








/* Function : square
 *
 * Purpose : calculates the square of each elements of an array
 *
 * Input Arguments : double *s <- the number array
 * 		     int N     <- the length of the array
 *
 * Output Arguments: double * <- the square of the elements of the input array
 *
 */
double *square(double *s, int N)
{
	int i;
	double *res = calloc(N, sizeof(double));

	if (s == NULL)
	{
		printf("square() : Input argument is NULL!\n");
		exit(EXIT_FAILURE);
	}

	if (N <= 0)
	{
		printf("square() : N must be positive!\n");
		exit(EXIT_FAILURE);
	}

	if (res == NULL)
	{
		printf("square() : Could not allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N; i++)
		res[i] = (double)sqrt((double)(s[i]));

	return res;
}








/* Function : sum
 *
 * Purpose : calculates the sum of the elements of an array
 *
 * Input Arguments : double *a <- the number array
 * 		     int N     <- the length of the array
 *
 * Output Arguments: double <- the sum of the elements of the input array
 *
 */
double sum(double *a, int N)
{
	int i;
	double res = 0.0;

	if (a == NULL)
	{
		printf("sum() : Input argument is NULL!\n");
		exit(EXIT_FAILURE);
	}

	if (N <= 0)
	{
		printf("sum() : N must be positive!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N; i++)
		res += a[i];

	return res;
}









/* Function : conjugate
 *
 * Purpose : calculates the conjugate of a complex array (changes the input array)
 *
 * Input Arguments : COMPLEX *A <- the complex number array
 * 		     int N      <- the length of the array
 *
 * Output Arguments: COMPLEX * <- the conjugate of the input array
 *
 */
COMPLEX *conjugate(COMPLEX *A, int N)
{
	int i;
	
	if (N <= 0)
	{
		printf("conjugate() : N must be positive!\n");
		exit(EXIT_FAILURE);
	}

	if (A == NULL)
	{
		printf("conjugate() : Input argument is NULL!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N; i++)
		A[i].imag = -A[i].imag;

	return A;
}








/* Function : isOdd
 *
 * Purpose : checks if a number is odd
 *
 * Input Arguments : int N <- the length of the array
 *
 * Output Arguments: int <- 1, if the array is of odd length
 * 			    0, otherwise
 *
 */
int isOdd(int N)
{
	if (N % 2 != 0)
		return 1;
	else
		return 0;
}







//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//												//
//												//
//	    			F I L T E R I N G   F U N C T I O N S				//
//												//
//												//
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////






/* Function : myconv
 *
 * Purpose : calculates the convolution of two complex arrays s1, s2 of size
 * 	     M and N, respectively
 *
 * Input Arguments : COMPLEX *s1 <- the first array to convolve
 * 		     COMPLEX *s2 <- the second array to convolve
 * 		     int N       <- the length of the s1-array
 * 		     int M       <- the length of the s2-array
 *
 * Output Arguments: double *X <- the result-array of the convolution
 *
 */
void myconv(COMPLEX *s1, COMPLEX *s2, int N, int M, COMPLEX *X)
{
	int i, j;

	if (s1 == NULL || s2 == NULL)
	{
		printf("myconv() : Input arguments are NULL!\n");
		exit(EXIT_FAILURE);
	}

	if (N <= 0 || M <= 0)
	{
		printf("myconv() : M and N must be positive!\n");
		exit(EXIT_FAILURE);
	}

	// Make sure the X vector is zero
	for (i = 0; i < N + M - 1; i++)
	{
		X[i].real = 0.0;
		X[i].imag = 0.0;
	}

	/* Convolution */
	for (i = 0; i < N; i++)
		for (j = 0; j < M; j++)
		{
			X[i+j].imag += (s1[i].imag * s2[j].real) + (s1[i].real * s2[j].imag);
			X[i+j].real += (s1[i].real * s2[j].real) - (s1[i].imag * s2[j].imag);
		}
}







/* Function : myconv_real
 *
 * Purpose: convolves two real-valued arrays s1, s2 of size N, M, respectively
 *
 * Input Arguments : double *s1 <- the first array to convolve
 * 		     double *s2 <- the second array to convolve
 * 		     int N      <- the length of the s1-array
 * 		     int M      <- the length of the s2-array
 *
 * Output Arguments: double *X <- the result-array of the convolution,
 *                                it is supposed to be preallocated
 *
 */
void myconv_real(double *s1, double *s2, int N, int M, double *X)
{
	int i, j;

	if (s1 == NULL || s2 == NULL)
	{
		printf("myconv_rea() : Input arguments are NULL!\n");
		exit(EXIT_FAILURE);
	}

	if (N <= 0 || M <= 0)
	{
		printf("myconv_real() : N and M must me positive!\n");
		exit(EXIT_FAILURE);
	}

	if (X == NULL)
	{
		printf("myconv_real() : Output arguments are NULL!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N + M - 1; i++)
		X[i] = 0.0;

	/* Convolution */
	for (i = 0; i < N; i++)
		for (j = 0; j < M; j++)
			X[i+j] += (s1[i] * s2[j]);
#ifdef DEBUG
	for (i = 0; i < N+M-1; i++)
		printf("X[%d] = %lf\n", i, X[i]);
#endif
}





/* Function : my_FIR_filter
 *
 * Purpose : filters the input array with an FIR filter described by the input arguments
 *
 * Input Arguments : double *B <- the filter nominator array
 * 		     double *X <- the input array
 * 		     int N     <- the length of the filter nominator array
 * 		     int M     <- the length of input array
 *
 * (Output Arguments:) 
 * 		     double *Y <- the result of the FIR filtering,
 *                                it is supposed to be preallocated
 */
void my_FIR_filter(double *B, double *X, int M, int N, double *Y)
{
	int i;

	if (B == NULL || X == NULL)
	{
		printf("my_FIR_filter() : Output arguments are NULL!\n");
		exit(EXIT_FAILURE);
	}

	if (M <= 0 || N <= 0)
	{
		printf("my_FIR_filter() : M and N must be positive!\n");
		exit(EXIT_FAILURE);
	}

	if (Y == NULL)
	{
		printf("my_FIR_filter() : Output arguments are NULL!\n");
		exit(EXIT_FAILURE);
	}

	myconv_real(B, X, M, N, Y);

	//for (i = 0; i < N; i++)
	//	printf("Y[%d] = %lf\n", i, Y[i]);
}







/* Function : my_IIR_filter
 *
 * Purpose : filters the input array with an IIR filter described by the input arguments
 *
 * Input Arguments : double *B <- the filter nominator array
 * 		     double *A <- the filter denominator array
 * 		     int M     <- the length of the filter nominator array
 *    		     int N     <- the length of the filter denominator array
 * 		     double *X <- the input array
 * 		     int K     <- the length of the input array
 *
 * (Output Arguments:) 
 * 		     double *Y <- the result of the FIR filtering,
 *                                it is supposed to be preallocated
 *
 */
void my_IIR_filter(double *B, double *A, int M, int N, double *X, int K, double *Y)
{
	int i, j;
	double FFV, FBV, tmp;

	if (B == NULL || A == NULL)
	{
		printf("my_IIR_filter() : Filter coefficients are NULL!\n");
		exit(EXIT_FAILURE);
	}
	if (X == NULL || Y == NULL)
	{
		printf("my_IIR_filter() : Input or Output arguments are NULL!\n");
		exit(EXIT_FAILURE);
	}
	if (N <= 0 || M <= 0 || K <= 0)
	{
		printf("my_IIR_filter() : M, N, K must be positive!\n");
		exit(EXIT_FAILURE);
	}

	if (A[0] != 1.0)
	{
		tmp = A[0];
		for (i = 0; i < N; i++)
		{
			A[i] = A[i]/tmp;
			//printf("A[%d] = %lf\n", i, A[i]);
		}
		for (i = 0; i < M; i++)
		{
			B[i] = B[i]/tmp;
			//printf("B[%d] = %lf\n", i, B[i]);
		}
	}

	for (i = 0; i < K; i++)
	{
		FFV = 0.0;
		FBV = 0.0;

		for (j = 1; j < N; j++)
		{
			if (i - j < 0)
				break;
			else
				FBV += (A[j] * Y[i-j]);
		}

		for (j = 0; j < M; j++)
		{
			if (i - j < 0)
				break;
			else
				FFV += (B[j] * X[i-j]);
		}
		
		Y[i] = FFV - FBV;
	}
}






/* Function : freqz
 *
 * Purpose : returns the complex frequency response at the frequencies designated 
 *           in vector freqV (in Hz), where Fs is the sampling frequency (in Hz).
 *
 *           Frequency response is given by : H(jw) = B(jw)/A(jw)
 *
 * Input Arguments : double *B     <- the nominator coefficients vector
 *                   double *A     <- the denominator coefficients vector
 *                   int M         <- the size of the nominator coefficents vector
 *                   int N         <- the size of the denominator coefficents vector
 *                   double *freqV <- the frequencies where the freq. response is evaluated
 *                   int frN       <- the size of the frequencies vector
 *                   double Fs     <- the sampling frequency
 *
 * Output Arguments: COMPLEX * <- the frequency response H(jw) = B(jw)/A(jw)
 */
COMPLEX *freqz(double *B, double *A, int M, int N, double *freqV, int frN, double Fs)
{
	double *a, *b;
	int n, i, L;
	double *digw;
	COMPLEX *s;
	COMPLEX *h1, *h2, *h;
	int flag;

	L = M-N;
	if (L >=0)
	{
		if ((a = calloc(N + L, sizeof(double))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}
		for(i = 0; i < N; i++)
			a[i] = A[i];
		flag = N+L;
	}
	else
	{
		if ((a = calloc(N, sizeof(double))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}
		for(i = 0; i < N; i++)
			a[i] = A[i];
		flag = N;
	}

	L = N-M;
	if (L >=0)
	{
		if ((b = calloc(M + L, sizeof(double))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}
		for(i = 0; i < M; i++)
			b[i] = B[i];
		flag = M+L;
	}
	else
	{
		if ((b = calloc(M, sizeof(double))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}
		for(i = 0; i < M; i++)
			b[i] = B[i];
		flag = M;
	}

	if ((digw = calloc(frN, sizeof(double))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}
	
	for (i = 0; i < frN; i++)
		digw[i] = 2*PI*freqV[i]/Fs;

	if ((s = calloc(frN, sizeof(COMPLEX))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < frN; i++)
	{
		s[i].real = cos(digw[i]);
		s[i].imag = sin(digw[i]);
#ifdef DEBUG
		printf("s[%d].real = %le , imag = %le\n", i, s[i].real, s[i].imag);
#endif
	}

	free(digw);

	h1 = polyval(b, N, s, frN);
	h2 = polyval(a, N, s, frN);

	free(s);

#ifdef DEBUG
	for (i = 0; i < frN; i++)
		printf("h1[%d] = %le + i*%le\n", i, h1[i].real, h1[i].imag);

	for (i = 0; i < frN; i++)
		printf("h2[%d] = %le + i*%le\n", i, h2[i].real, h2[i].imag);
#endif
	if ((h = calloc(frN, sizeof(COMPLEX))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < frN; i++)
	{
		h[i].real = (h1[i].real * h2[i].real + h1[i].imag * h2[i].imag)/(h2[i].real * h2[i].real + h2[i].imag * h2[i].imag);
		h[i].imag = (h2[i].real * h1[i].imag - h2[i].imag * h1[i].real)/(h2[i].real * h2[i].real + h2[i].imag * h2[i].imag);

	}

	free(h1);
	free(h2);
	free(a);
	free(b);

	return h;
}







/* Function : frequency_shift
 *
 * Purpose : Performs frequency shifting
 *
 * Input Arguments : double *F <- frequency vector in Hz
 *  		     int L     <- size of frequency vector
 *  		     double df <- frequency shift
 *  		     double Fs <- sampling rate
 *  		     char *options <- controls the usage of the function
 *  			 	     "linear" : linear shift in Hz
 *  			             "bark"   : shift is made in Bark scale (i.e. -400 to +400)
 *
 * Output Arguments: double *FS <- frequency shifting vector
 */
double *frequency_shift(double *F, int L, double df, double Fs, char *options)
{
	int K, i;
	double C, expAB;
	double *FS = calloc(L, sizeof(double));

	if (FS == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	if (strstr(options, "bark") != NULL)
	{
		K = 1000.0;
		C = 1000.0/log10(2);
		expAB = exp(-df/C);

		for (i = 0; i < L; i++)
			FS[i] = F[i]*expAB + K*(expAB-1);
	}
	else if (strstr(options, "linear:shift") != NULL)
	{
		// FS = max(0, min(F-df, Fs/2));
		for (i = 0; i < L; i++)
		{
			if ((F[i] - df) > Fs/2)
				FS[i] = Fs/2;
			else
				FS[i] = F[i] - df;

			if (FS[i] < 0.0)
				FS[i] = 0.0;
		}
	}
	else if (strstr(options, "linear:scale") != NULL)
	{
		// FS = max(0, min(F/df, Fs/2));
		for (i = 0; i < L; i++)
		{
			if ((F[i] / df) > Fs/2)
				FS[i] = Fs/2;
			else
				FS[i] = F[i] / df;

			if (FS[i] < 0.0)
				FS[i] = 0.0;
		}
	}
	else
	{
		printf("Option string %s is not recognized!\n", options);
		exit(EXIT_FAILURE);
	}

	return FS;
}






/* Function : notch_filter
 *
 * Purpose: creates a notch filter which cuts off a selected frequency (DC) from a signal
 *
 * Input Arguments : double cutoff_freq <- the filter cutoff frequency
 * 		     double Fs          <- the sampling frequency
 *
 * Output Arguments: double *B <- the nominator coefficients
 *                   double *A <- the denominator coefficients
 *
 */
void notch_filter(double cutoff_freq, double Fs, COMPLEX *B, COMPLEX *A)
{
	COMPLEX *b = NULL;
	COMPLEX *a = NULL;

	if (B == NULL || A == NULL)
	{
		printf("notch_filter() : Output arguments are null.\n");
		exit(EXIT_FAILURE);
	}

	if (cutoff_freq < 0 || Fs <= 0)
	{
		Fs = 16000.0;
		cutoff_freq = 0.0;
	}

	if (cutoff_freq == 0.0)
	{
		b = calloc(2, sizeof(COMPLEX));
		a = calloc(2, sizeof(COMPLEX));

		if (a == NULL || b == NULL)
		{
			printf("Could not allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		b[0].real = 1.0; 
		b[0].imag = 0.0;
		b[1].real = -0.999; 
		b[1].imag = 0.0;
		a[0].real = 1.0; 
		a[0].imag = 0.0;
		a[1].real = -0.995*cos(2*PI*10/Fs);
		a[1].imag = -0.995*sin(2*PI*10/Fs);

		myconv(b, conjugate(b,2), 2, 2, B);
		myconv(a, conjugate(a,2), 2, 2, A);

		//printf("B[0].real = %.15lf , B[0].imag = %.15lf\n", B[0].real, B[0].imag);
		
		//printf("A[1].real = %.15lf , A[1].imag = %.15lf\n", A[1].real, A[1].imag);
	}
	else
	{
		printf("\nnotch_filter() : Non-DC notch not implemented yet.\n");
		exit(EXIT_FAILURE);
	}

	free(b);
	free(a);
}







/* Function : deconv
 *
 * Purpose : deconvolves vector A out of vector B. If A and B are vectors 
 *           of polynomial coefficients, deconvolution is equivalent to polynomial division.  
 *           The result of dividing B by A is quotient Q and remainder R.
 *
 * Input Arguments : double *B <- the first polynomial coefficients vector
 *                   int NB    <- the size of B vector
 *                   double *A <- the second polynomial coefficents vector
 *                   int NA    <- the size of A vector
 *
 * Output Arguments: double * <- the deconvolution result
 */
double *deconv(double *B, int NB, double *A, int NA)
{
	double *q;
	double *temp;
	int i;

	if (A[0] == 0.0)
	{
		printf("deconv() : first coefficient of A must be non-zero!\n");
		exit(EXIT_FAILURE);
	}
	
	if (NA > NB)
	{
		q = calloc(1, sizeof(double));
		q[0] = 0.0;
	}
	else
	{
		temp = calloc(NB-NA + 1, sizeof(double));
		temp[0] = 1.0;
		q = calloc(NB-NA + 1, sizeof(double));

		my_IIR_filter(B, A, NB, NA, temp, NB-NA+1, q);
		free(temp);
	}

	return q;
}








//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//												//
//												//
//	    		P O L Y N O M I A L   R E L A T E D   F U N C T I O N S			//
//												//
//												//
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////









/* Function : laguer
 *
 * Purpose : given the degree m and the m+1 complex coefficients a[0..m] of the polynomial
 *           sum (i=0 -> m) a[i] * x^i, and given a complex value x, this routine improves x
 *           by Laguerre's method until it converges, within the achievable roundoff limit,
 *           to a root of the given polynomial. The number of iterations taken is returned as its
 *
 * Input Arguments : complex *a <- the polynomial coefficients (m+1)
 *                   int m      <- the degree of the polynomial
 *(Output Arguments: )
 *                   complex *x <- an initial guess for a root (input)
 *                                 a root of the polynomial (output)
 *                   int *its   <- the number of iterations
 *                    
 * Taken from : Numerical Recipes in C, 1992.
 */
void laguer(COMPLEX *a, int m, COMPLEX *x, int *its)
{
	int iter, j;
	double abx, abp, abm, err;
	COMPLEX dx, x1, b, d, f, g, h, sq, gp, gm, g2;
	static double frac[MR+1] = {0.0, 0.5, 0.25, 0.75, 0.13, 0.38, 0.62, 0.88, 1.0};

	for (iter = 1; iter <= MAXIT; iter++)
	{
		*its = iter;
		b = a[m];
		err = Cabs(b);
		d = f = CComplex(0.0, 0.0);
		abx = Cabs(*x);

		for (j = m-1; j >= 0; j--)
		{
			f = Cadd(Cmul(*x, f), d);
			d = Cadd(Cmul(*x, d), b);
			b = Cadd(Cmul(*x, b), a[j]);
			err = Cabs(b) + abx*err;
		}

		err *= EPSS;

		if (Cabs(b) <= err)
			return;

		g = Cdiv(d, b);
		g2 = Cmul(g, g);
		h = Csub(g2, RCmul(2.0, Cdiv(f, b)));
		sq = Csqrt(RCmul((double)(m-1), Csub(RCmul((double)m, h), g2)));
		gp = Cadd(g, sq);
		gm = Csub(g, sq);
		abp = Cabs(gp);
		abm = Cabs(gm);

		if (abp < abm)
			gp = gm;
		
		dx = ((max_L(abp, abm) > 0.0 ? Cdiv(CComplex((double)m, 0.0), gp): RCmul(1 + abx, CComplex(cos((double)iter), sin((double)iter)))));
		
		x1 = Csub(*x, dx);

		if (x->real == x1.real && x->imag == x1.imag)
			return;
		if (iter % MT)
			*x = x1;
		else
			*x = Csub(*x, RCmul(frac[iter/MT], dx));
	}

	printf("laguer() : too many iterations in laguer!\n");
	return;
}








/* Function : zroots
 *
 * Purpose : given the degree m and the m+1 complex coefficients a[0..m] of the polynomial
 *           sum (i = 0 -> m) a[i] * x^i, this routine successively calls laguer and finds
 *           all m complex roots in r[1..m]. The boolean variable polish should be input
 *           as true (1) if polishing (also by Laguerre's method) is desired, false (0) if
 *           the roots will be subsequently polished by other means
 *
 * Input Arguments : COMPLEX *a <- the polynomial coefficients (m+1)
 *                   int m      <- the degree of the polynomial
 *                   int polish <- 1 for polishing, 0 otherwise
 *(Output Arguments: )
 *                   COMPLEX *r <- the roots of the polynomial
 *
 * Taken from : Numerical Recipes in C, 1992.
 */
void zroots(COMPLEX *a, int m, COMPLEX *r, int polish)
{
	int i, its, j, jj;
	COMPLEX x, b, c, ad[MAXM];

	for (j = 0; j <= m; j++)
		ad[j] = a[j];
	for (j = m; j >= 1; j--)
	{
		x = CComplex(0.0, 0.0);
		laguer(ad, j, &x, &its);
		
		if (fabs(x.imag) <= 2.0*EPS*fabs(x.real))
			x.imag = 0.0;
		r[j] = x;
		b = ad[j];

		for (jj = j - 1; jj >= 0; jj--)
		{
			c = ad[jj];
			ad[jj] = b;
			b = Cadd(Cmul(x, b), c);
		}
	}

	if (polish)
		for (j = 1; j <= m; j++)
			laguer(a, m, &r[j], &its);

	for (j = 2; j <= m; j++)
	{
		x = r[j];

		for (i = j - 1; i >= 1; i--)
		{
			if (r[i].real <= x.real)
				break;

			r[i+1] = r[i];
		}

		r[i+1] = x;
	}
}










/* Function : zrhqr
 *
 * Purpose : find all the roots of a polynomial with real coefficients, sum (i = 0 -> m) a(i) * x^i,
 *           given the degree m and the coefficients a[0..m]. The method is to construct an upper
 *           Hessenberg matrix whose eigenvalues are the desired roots, and then use the routines
 *           balanc and hqr. The real and imaginary parts of the roots are returned in rtr[1..m]
 *           and rti[1..m], respectively
 *
 * Input Arguments : double *a   <- the polynomial coefficients
 *                   int m       <- the size of the polynomial coefficients vector
 *
 * Output Arguments:
 *                   double *rtr <- the real part of the roots
 *                   double *rti <- the imaginary part of the roots
 *
 * Taken from: Numerical Recipes in C, 1992.
 */
void zrhqr(double *a, int m, double *rtr, double *rti)
{
	int j,k;
	double **hess, xr, xi;

	hess = calloc(MAXM_2, sizeof(double *));
	
	for (j = 0; j < MAXM_2; j++)
		hess[j] = calloc(MAXM_2, sizeof(double));

	// Shifting matrix pointer back for compliance with the algorithm
	for (j = 0; j < MAXM_2; j++)
		*(hess + j) = *(hess + j) - 1;

	hess = hess - 1;

	if (m > MAXM_2 || a[m] == 0.00000)
	{
		printf("zrhqr() : Bad args in zrhqr!\n");
		exit(EXIT_FAILURE);
	}

	for (k = 1; k <= m; k++)
	{
		hess[1][k] = -a[m-k]/a[m];
		
		for (j = 2; j <= m; j++)
			hess[j][k] = 0.0;

		if (k != m)
			hess[k+1][k] = 1.0;
	}

#ifdef DEBUG
	for (j = 1; j <= m; j++)
		for (k = 1; k <= m; k++)
			printf("hess[%d][%d] = %lf\n", j, k, hess[j][k]);
#endif
	balanc(hess, m);
	hqr(hess, m, rtr, rti);

	for (j = 2; j <= m; j++)
	{
		xr = rtr[j];
		xi = rti[j];

		for (k = j-1; k >= 1; k--)
		{
			if (rtr[k] <= xr)
				break;

			rtr[k+1] = rtr[k];
			rti[k+1] = rti[k];
		}

		rtr[k+1] = xr;
		rti[k+1] = xi;
	}

	// Shifting back matrix to normal state and free

	hess = hess + 1;

	for (j = 0; j < MAXM_2; j++)
		*(hess + j) = *(hess + j) + 1;

	for (j = 0; j < MAXM_2; j++)
	{
		free(hess[j]);
		hess[j] = NULL;
	}

	free(hess);

#ifdef DEBUG
	printf("Roots found and exiting...\n");
#endif
}










/* Function : balanc
 *
 * Purpose : given a matrix a[1..n][1..n], this routine replaces it by a balanced matrix
 *           with identical eigenvalues. A symmetric matrix is already balanced and is unaffected
 *           by this procedure. The parameter RADIX should be the machine's floating-point
 *           radix
 *
 * Input Arguments : double **a <- the NxN matrix
 *                   int n      <- the matrix size
 *
 * Output Arguments : -
 *
 * Taken from : Numerical Recipes in C, 1992.
 */
void balanc(double **a, int n)
{
	int last, j, i;
	double s, r, g, f, c, sqrdx;

	sqrdx = RADIX * RADIX;
	last = 0;

	while (last == 0)
	{
		last = 1;

		for (i = 1; i <= n; i++)
		{
			r = c = 0.0;

			for (j = 1; j <= n; j++)
			{
				if (j != i)
				{
					c += fabs(a[j][i]);
					r += fabs(a[i][j]);
				}
			}
			if (c != 0.0 && r != 0.0)
			{
				g = r / RADIX;
				f = 1.0;
				s = c + r;

				while (c < g)
				{
					f *= RADIX;
					c *= sqrdx;
				}
				g = r * RADIX;

				while (c > g)
				{
					f /= RADIX;
					c /= sqrdx;
				}
				if ((c + r)/f < 0.95*s)
				{
					last = 0;
					g = 1.0/f;

					for (j = 1; j <= n; j++)
						a[i][j] *= g;
					for (j = 1; j <= n; j++)
						a[j][i] *= f;
				}
			}
		}
	}
}








/* Function : hqr
 *
 * Purpose : finds all eigenvalues of an upper Hessenberg matrix a[1..n][1..n]. On output,
 *           a is destroyed. The real and imaginary parts of the eigenvalues are returned
 *           in wr[1..n] and wi[1..n], respectively
 *
 * Input Arguments : double **a <- the matrix a from function balanc( )
 *                   int n      <- the size of the matrix
 *
 *(Output Arguments: )
 *                   double *wr <- the real part of the solution vector
 *                   double *wi <- the imaginary part of the solution vector
 * 
 * Taken from : Numerical Recipes in C, 1992.
 */
void hqr(double **a, int n, double *wr, double *wi)
{
	int nn, m, l, k, j, its, i, mmin;
	double z, y, x, w, v, u, t, s, r, q, p, anorm;

	anorm = 0.0;

	for (i = 1; i <= n; i++)
		for (j = (int)max_L((double)i-1.0, 1.0); j <= n; j++)
			anorm += fabs(a[i][j]);

	nn = n;
	t = 0.0;

	while (nn >= 1)
	{
		its = 0;
		
		do {
			for (l = nn; l >= 2; l--)
			{
				s = fabs(a[l-1][l-1]) + fabs(a[l][l]);
				if (s == 0.0)
					s = anorm;
				if ((double)(fabs(a[l][l-1]) + s) == s)
				{
					a[l][l-1] = 0.0;
					break;
				}
			}
			x = a[nn][nn];
			if (l == nn)
			{
				wr[nn] = x + t;
				wi[nn--] = 0.0;
			}
			else
			{
				y = a[nn-1][nn-1];
				w = a[nn][nn-1] * a[nn-1][nn];
				if (l == (nn-1))
				{
					p = 0.5 * (y-x);
					q = p * p + w;
					z = sqrt(fabs(q));
					x += t;
					if (q >= 0.0)
					{
						z = p + SIGN(z,p);
						wr[nn-1] = wr[nn] = x+z;
						if (z)
							wr[nn] = x-w/z;
						wi[nn-1] = wi[nn] = 0.0;
					}
					else
					{
						wr[nn-1] = wr[nn] = x+p;
						wi[nn-1] = -(wi[nn] = z);
					}
					nn -= 2;
				}
				else
				{
					if (its == 50)
					{
						printf("hqr() : Too many iterations in hqr!\n");
						exit(EXIT_FAILURE);
					}
					if (its == 10 || its == 20)
					{
						t += x;
						for (i = 1; i <= nn; i++)
							a[i][i] -= x;
						s = fabs(a[nn][nn-1]) + fabs(a[nn-1][nn-2]);
						y = x = 0.75*s;
						w = -0.4375*s*s;
					}
					++its;

					for (m = (nn-2); m >= l; m--)
					{
						z = a[m][m];
						r = x-z;
						s = y-z;
						p = (r*s-w)/a[m+1][m] + a[m][m+1];
						q = a[m+1][m+1] - z - r - s;
						r = a[m+2][m+1];
						s = fabs(p) + fabs(q) + fabs(r);
						p /= s;
						q /= s;
						r /= s;

						if (m == l)
							break;
						u = fabs(a[m][m-1])*(fabs(q)+fabs(r));
						v = fabs(p)*(fabs(a[m-1][m-1]) + fabs(z) + fabs(a[m+1][m+1]));
						if ((double)(u+v) == v)
							break;
					}
					for (i = m + 2; i <= nn; i++)
					{
						a[i][i-2] = 0.0;
						if (i != (m + 2))
							a[i][i-3] = 0.0;
					}
					for (k = m; k <= nn-1; k++)
					{
						if (k != m)
						{
							p = a[k][k-1];
							q = a[k+1][k-1];
							r = 0.0;
							if (k != (nn-1))
								r = a[k+2][k-1];
							if ((x = fabs(p) + fabs(q) + fabs(r)) != 0.0)
							{
								p /= x;
								q /= x;
								r /= x;
							}
						}
						
						if ((s = SIGN(sqrt(p*p + q*q + r*r), p)) != 0.0)
						{
							if (k == m)
							{
								if (l != m)
									a[k][k-1] = -a[k][k-1];
							}else
								a[k][k-1] = -s*x;

							p += s;
							x = p/s;
							y = q/s;
							z = r/s;
							q /= p;
							r /= p;

							for (j = k; j <= nn; j++)
							{
								p = a[k][j] + q*a[k+1][j];
								if (k != (nn-1))
								{
									p += r*a[k+2][j];
									a[k+2][j] -= p*z;
								}
								a[k+1][j] -= p*y;
								a[k][j] -= p*x;
							}
							mmin = nn < k + 3 ? nn : k + 3;

							for (i = l; i <= mmin; i++)
							{
								p = x*a[i][k] + y*a[i][k+1];

								if (k != (nn-1))
								{
									p += z*a[i][k+2];
									a[i][k+2] -= p*r;
								}
								a[i][k+1] -= p*q;
								a[i][k] -= p;
							}
						}
					}
				}
			}
		} while (l < nn-1);
	}
}
	









/* Function : roots
 *
 * Purpose : finds the roots of a polynomial, with its coefficients given in A, in ascending order
 *
 * Input Arguments : double *A   <- the polynomial coefficents vector
 *                   int N       <- the size of the A vector
 *
 * (Output Arguments: )
 *                   COMPLEX *xx <- the complex roots of the polynomial
 */
void roots(double *A, int N, COMPLEX *xx)
{
	int i; 
	int odr = N-1;
	int a_zero = 0;
	double eps = 10e-10;
	int itrat = 100;
	COMPLEX *x = calloc(N, sizeof(COMPLEX));

	//root_pol(A, odr, x, a_zero, eps, itrat);

	for (i = 0; i < N-1; i++)
	{
		xx[i].real = x[i+1].real;
		xx[i].imag = x[i+1].imag;
	}

#ifdef DEBUG
	for (i = 0; i < N-1; i++)
		printf("xx[%d] = %lf + i*%lf\n", i, xx[i].real, xx[i].imag);
#endif

	free(x);
}







/* Function : polyval
 *
 * Purpose : returns the value of a polynomial P evaluated at X. P
 * 	     is a vector of length N+1 whose elements are the coefficients 
 * 	     of the polynomial in descending powers.
 *
 *           Y = P(1)*X^N + P(2)*X^(N-1) + ... + P(N)*X + P(N+1)
 *
 * Input Arguments : double *P   <- the polynomial coefficients in descending powers
 *                   int L       <- the size of the P vector
 *                   COMPLEX *xi <- the evaluating points
 *                   int I       <- the size of vector xi
 *
 * Output Arguments: COMPLEX * <- the values of the polynomial P at points xi
 */
COMPLEX *polyval(double *P, int L, COMPLEX *xi, int I)
{
	int nc;
	COMPLEX *y = NULL;
	int siz_x;
	int i, j;
	double tmp;

	nc = L;
	siz_x = I;

	// Use Horner's method for general case where X is an array.
	if ((y = calloc(siz_x, sizeof(COMPLEX))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	if (nc > 0)
	{
	       for (i = 0; i < siz_x; i++)
	       {
		       y[i].real = P[0];
		       //y[i].imag = 0.0; // unecessary because of calloc
	       }
	}

	for (i = 1; i < nc; i++)
	{
		for (j = 0; j < siz_x; j++)
		{
			tmp = (xi[j].real * y[j].real) - (xi[j].imag * y[j].imag) + P[i];
			y[j].imag = (xi[j].real * y[j].imag) + (xi[j].imag * y[j].real);
			y[j].real = tmp;
		}
#ifdef DEBUG
		for (j = 0; j < siz_x; j++)
			printf("y[%d] = %le + i*%le\n", j, y[j].real, y[j].imag);	
#endif
	}

	return y;
}








/* Function : mypoly
 *
 * Purpose : converts roots to polynomial (coefficients)
 *
 * Input Arguments : COMPLEX *x <- the roots of the polynomial (vector)
 * 		     int N      <- the length of the roots' array
 *
 * Output Arguments: double *y <- the polynomial coefficients
 *
 * Reference:
 * A.M. Kondoz, "Digital Speech: Coding for Low Bit Rate Communications
 * Systems" John Wiley & Sons 1994 ,Chapter 4 
 *
 */
void mypoly(COMPLEX *x, int N, double *y)
{
	int i = 0, j = 0, k = 0;
	COMPLEX *c = NULL, *tmp = NULL;
	double t1 = 0.0, t2 = 0.0, t3 = 0.0, t4 = 0.0;

	if (x == NULL)
	{
		printf("mypoly() : Input argument is NULL!\n");
		exit(EXIT_FAILURE);
	}

	if (N <= 0)
	{
		printf("mypoly() : N should be positive!\n");
		exit(EXIT_FAILURE);
	}

	/* Expand recursion formula */
	if ((c = calloc((N+1), sizeof(COMPLEX))) == NULL)
	{
		printf("Could not allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	c[0].real = 1.0;
	c[0].imag = 0.0;

	if ((tmp = calloc(N, sizeof(COMPLEX))) == NULL)
	{
		printf("Could not allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	for (j = 0; j < N; j++)
	{
		for (k = 0; k <= j; k++)
		{
			tmp[k].real = c[k].real;
			tmp[k].imag = c[k].imag;
		}

		for (i = 1; i <= j+1; i++)
		{
			c[i].real -= (x[j].real * tmp[i-1].real - x[j].imag * tmp[i-1].imag);
			c[i].imag -= (x[j].imag * tmp[i-1].real + x[j].real * tmp[i-1].imag);
		}
	}

	free(tmp);

	// The result should be real if the roots are complex conjugates.
	for (i = 0; i < N+1; i++)
		y[i] = c[i].real;

	free(c);
}









/* Function : poly2lsf
 *
 * Purpose : converts the prediction polynomial specified by A, into the corresponding 
 *           line spectral frequencies, LSF
 *
 * Input Arguments : double *A <- the prediction polynomial coeffients vector
 *                   int N     <- the size of the A vector
 *
 * Output Arguments: double * <- the line spectral frequencies vector
 */
double *poly2lsf(double *A, int N)
{
	int i = 0, Ord = 0;
	double *lsf = NULL, *rtemp = NULL, *a1 = NULL, *a2 = NULL, *P1 = NULL, *Q1 = NULL;
	double maxelem = 0.0, tmp = 0.0, *rtr = NULL, *rti = NULL;
	double *P = NULL, *Q = NULL, *aP = NULL, *aQ = NULL;
	COMPLEX *c = NULL, *rP = NULL, *rQ = NULL, *temp = NULL;
	double *a = NULL;
	
	if (A[0] != 1.000)
	{
		tmp = A[0];
		for (i = 0; i < N; i++)
			A[i] = A[i]/tmp;
	}

	if ((c = calloc(N-1, sizeof(COMPLEX))) == NULL || (temp = calloc(N, sizeof(COMPLEX))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}
	
	if ((rtr = calloc(N-1, sizeof(double))) == NULL || (rti = calloc(N-1, sizeof(double))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	if ((a = calloc(N, sizeof(COMPLEX))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N; i++)
		a[i] = A[N - 1 - i];

	rtr = rtr - 1;
	rti = rti - 1;

	zrhqr(a, N-1, rtr, rti);

	rtr = rtr + 1;
	rti = rti + 1;

	for (i = 0; i < N-1; i++)
	{
		c[i].real = rtr[i];
		c[i].imag = rti[i];
		//printf("Roots are %lf + i*%lf\n", c[i].real, c[i].imag);
	}

	rtemp = CabsVal(c, N-1);
	free(c);
	c = NULL;
	maxelem = maxEl(rtemp, N-1);
	free(rtemp);
	rtemp = NULL;

	free(rtr);
	rtr = NULL;
	free(rti);
	rti = NULL;
	free(a);
	a = NULL;

#ifdef POLY2LSF_DEBUG
	printf("max = %lf \n", maxelem);
#endif

	if (maxelem >= 1.0)
	{
		printf("poly2lsf() : The polynomial must have all its roots inside the unit circle!\n");
		exit(EXIT_FAILURE);
	}

	// Sum and difference filters
	Ord = N - 1;
	if ((a1 = calloc(N + 1, sizeof(double))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N; i++)
		a1[i] = A[i];
	a1[N] = 0.0;

	if ((a2 = calloc(N + 1, sizeof(double))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N + 1; i++)
		a2[i] = a1[N - i];

	if ((P1 = calloc(N + 1, sizeof(double))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	if ((Q1 = calloc(N + 1, sizeof(double))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N + 1; i++)
	{
		P1[i] = a1[i] - a2[i];
		Q1[i] = a1[i] + a2[i];
	}

	free(a1);
	free(a2);

	// If the order is even, then remove the known root at z=1
	// for P1 and z=-1 for Q1
	// If the order is odd, then remove both the roots from P1
	
	if (isOdd(Ord) == 1)
	{
		printf("is odd!\n");
		if ((rtemp = calloc(3, sizeof(double))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		rtemp[0] = 1.0; rtemp[1] = 0.0; rtemp[2] = -1.0;
		
		P = deconv(P1, N+1, rtemp, 3);

		free(rtemp);
		
		Q = Q1;
		
		if ((rP = calloc(N-2, sizeof(COMPLEX))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}
		
		if ((rQ = calloc(N, sizeof(COMPLEX))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		if ((rtr = calloc(N-2, sizeof(double))) == NULL || (rti = calloc(N-2, sizeof(double))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		rtr = rtr - 1;
		rti = rti - 1;

		zrhqr(P, N-2, rtr, rti);

		rtr = rtr + 1;
		rti = rti + 1;

		for (i = 0; i < N-2; i++)
		{
			rP[i].real = rtr[i];
			rP[i].imag = rti[i];
		}

		free(rtr);
		free(rti);


		if ((rtr = calloc(N, sizeof(double))) == NULL || (rti = calloc(N, sizeof(double))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		rtr = rtr - 1;
		rti = rti - 1;

		zrhqr(Q, N, rtr, rti);

		rtr = rtr + 1;
		rti = rti + 1;

		for (i = 0; i < N; i++)
		{
			rQ[i].real = rtr[i];
			rQ[i].imag = rti[i];
		}

		free(rtr);
		free(rti);

#ifdef POLY2LSF_DEBUG
		for (i = 0; i < N; i++)
			printf("rQ[%d] = %lf + %lf\n", i, rQ[i].real, rQ[i].imag);
#endif
		free(P);
		free(Q);
		free(P1);
		
		if ((temp = calloc(N/2, sizeof(COMPLEX))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		sortC(rP, N-2);
#ifdef POLY2LSF_DEBUG
		for (i = 0; i < N-2; i++)
			printf("rP = %lf, i = %lf\n", rP[i].real, rP[i].imag);
#endif
		sortC(rQ, N);
#ifdef POLY2LSF_DEBUG
		for (i = 0; i < N; i++)
			printf("rQ = %lf, i = %lf\n", rQ[i].real, rQ[i].imag);
#endif
		for (i = 0; i < N/2 - 1; i++)
		{
			temp[i].real = rP[2*i].real;
			if (rP[2*i].imag < 0.00)
				temp[i].imag = -rP[2*i].imag;
			else
				temp[i].imag = rP[2*i].imag;
		}
		
		aP = angle(temp, N/2 - 1);
#ifdef POLY2LSF_DEBUG
		for (i = 0; i < N/2 - 1; i++)
			printf("aP[%d] = %lf\n", i, aP[i]);
#endif

		free(temp);

		if ((temp = calloc(N/2, sizeof(COMPLEX))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		for (i = 0; i < N/2; i++)
		{
			temp[i].real = rQ[2*i].real;
			if (rQ[2*i].imag < 0.00)
				temp[i].imag = -rQ[2*i].imag;
			else
				temp[i].imag = rQ[2*i].imag;
		}
		
		aQ = angle(temp, N/2);
#ifdef POLY2LSF_DEBUG
		for (i = 0; i < N/2; i++)
			printf("aQ[%d] = %lf\n", i, aQ[i]);
#endif

		free(temp);
		free(rP);
		free(rQ);

		if ((rtemp = calloc(N/2 - 1 + N/2, sizeof(double))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		for (i = 0; i < N/2 - 1; i++)
			rtemp[i] = aP[i];
		for (i = N/2 - 1; i < N-1; i++)
			rtemp[i] = aQ[i - N/2 + 1];

		sort(rtemp, N-1);
		free(aP);
		free(aQ);
		lsf = rtemp;
		
	}
	else
	{
		if ((rtemp = calloc(2, sizeof(double))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		rtemp[0] = 1.0; rtemp[1] = -1.0;
		
		P = deconv(P1, N+1, rtemp, 2);
		free(rtemp);

		if ((rP = calloc(N-1, sizeof(COMPLEX))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		if ((rtr = calloc(N-1, sizeof(double))) == NULL || (rti = calloc(N-1, sizeof(double))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		rtr = rtr - 1;
		rti = rti - 1;

		zrhqr(P, N-1, rtr, rti);

		rtr = rtr + 1;
		rti = rti + 1;

		for (i = 0; i < N-1; i++)
		{
			rP[i].real = rtr[i];
			rP[i].imag = rti[i];
		}

		free(rtr);
		free(rti);
		
		if ((rtemp = calloc(2, sizeof(double))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		rtemp[0] = 1.0; rtemp[1] = 1.0;
		Q = deconv(Q1, N+1, rtemp, 2);
		
		if ((rQ = calloc(N-1, sizeof(COMPLEX))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		if ((rtr = calloc(N-1, sizeof(double))) == NULL || (rti = calloc(N-1, sizeof(double))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		rtr = rtr - 1;
		rti = rti - 1;
		
		zrhqr(Q, N-1, rtr, rti);

		rtr = rtr + 1;
		rti = rti + 1;

		for (i = 0; i < N-1; i++)
		{
			rQ[i].real = rtr[i];
			rQ[i].imag = rti[i];
		}

		free(rtr);
		free(rti);

#ifdef POLY2LSF_DEBUG
		for (i = 0; i < N-1; i++)
			printf("rP unsorted = %lf, i = %lf\n", rP[i].real, rP[i].imag);
		for (i = 0; i < N; i++)
			printf("P[%d] right after roots call = %lf\n", i, P[i]);
		for (i = 0; i < N; i++)
			printf("Q = %lf\n", Q[i]);
		for (i = 0; i < N-1; i++)
			printf("rQ unsorted = %lf, i = %lf\n", rQ[i].real, rQ[i].imag);
#endif
		
		free(P);
		free(Q);
		free(P1);
		free(Q1);

		sortC(rP, N-1);
#ifdef POLY2LSF_DEBUG
		for (i = 0; i < N-1; i++)
			printf("rP = %lf, i = %lf\n", rP[i].real, rP[i].imag);
#endif
		sortC(rQ, N-1);
#ifdef POLY2LSF_DEBUG
		for (i = 0; i < N-1; i++)
			printf("rQ = %lf, i = %lf\n", rQ[i].real, rQ[i].imag);
#endif
		if ((temp = calloc((N+1)/2 - 1, sizeof(COMPLEX))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		for (i = 0; i < (N+1)/2 - 1; i++)
		{
			temp[i].real = rP[2*i].real;
			if (rP[2*i].imag < 0.00)
				temp[i].imag = -rP[2*i].imag;
			else
				temp[i].imag = rP[2*i].imag;
		}
		
		aP = angle(temp, (N+1)/2 - 1);
#ifdef POLY2LSF_DEBUG
		for (i = 0; i < (N+1)/2 - 1; i++)
			printf("aP[%d] = %lf\n", i, aP[i]);
#endif
		free(temp);

		if ((temp = calloc((N+1)/2 - 1, sizeof(COMPLEX))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		for (i = 0; i < (N+1)/2 - 1; i++)
		{
			temp[i].real = rQ[2*i].real;
			if (rQ[2*i].imag < 0.00)
				temp[i].imag = -rQ[2*i].imag;
			else
				temp[i].imag = rQ[2*i].imag;
		}
		
		aQ = angle(temp, (N+1)/2 - 1);
#ifdef POLY2LSF_DEBUG
		for (i = 0; i < (N+1)/2 - 1; i++)
			printf("aQ[%d] = %lf\n", i, aQ[i]);
#endif
		free(temp);

		free(rP);
		free(rQ);

		if ((rtemp = calloc(N - 1, sizeof(double))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		for (i = 0; i < (N-1)/2; i++) // ?
			rtemp[i] = aP[i];
		for (i = (N-1)/2; i < N - 1; i++) // ?
			rtemp[i] = aQ[i - (N-1)/2];

		sort(rtemp, N-1);
		free(aP);
		free(aQ);
		lsf = rtemp;
	}

	return lsf;
}








/* Function: lsf2poly
 *
 * Input Arguments: double *lsf <- the Line Spectral Frequencies vector
 * 		    int N       <- the size of the lsf vector
 *
 * Output Arguments: double *a <- the resulting prediction polynomial coefficients
 *
 *   Reference:
 *   A.M. Kondoz, "Digital Speech: Coding for Low Bit Rate Communications
 *   Systems" John Wiley & Sons 1994 ,Chapter 4 
 */
void lsf2poly(double *lsf , int N, double *a)
{
	int p = 0, i = 0;
	COMPLEX *z = NULL, *rQ = NULL, *rP = NULL;
	double *P1 = NULL, *Q1 = NULL;
	double *P = NULL, *Q = NULL, *tmp = NULL;
	double m = 0.0, n = 0.0;

	if (lsf == NULL)
	{
		printf("lsf2poly() : Input arguments are NULL!\n");
		exit(EXIT_FAILURE);
	}
	if (N <= 0)
	{
		printf("lsf2poly() : N must be positive!\n");
		exit(EXIT_FAILURE);
	}
	if (a == NULL)
	{
		printf("lsf2poly() : Output arguments are NULL!\n");
		exit(EXIT_FAILURE);
	}

	m = maxEl(lsf, N);
	n = minEl(lsf, N);

	if (m > PI || n < 0)
	{
		//printf("%lf - %lf\n", m, n);
		printf("lsf2poly() : Line spectral frequencies must be between 0 and pi.\n");
		exit(EXIT_FAILURE);
	}

	p = N; // this is the model order

	// Form zeros using the LSFs and unit amplitudes

	if ((z = calloc(N, sizeof(COMPLEX))) == NULL)
	{
		printf("Could not allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N; i++)
	{
		z[i].real = cos(lsf[i]);
		z[i].imag = sin(lsf[i]);
		//printf("z[%d] = %.20lf + %.20lf\n", i, z[i].real, z[i].imag);
	}
	//printf("\n\n");
	
	/* ------------- PERHAPS IT CAN BE OPTIMIZED! (some loops can be merged into one) --------- */
	

	if (isOdd(N) == 1)
	{
		rQ = calloc((N+1), sizeof(COMPLEX));
		rP = calloc((N-1), sizeof(COMPLEX));

		if (rQ == NULL || rP == NULL)
		{	
			printf("Could not allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		// Separate the zeros to those belonging to P and Q
		for (i = 0; i < (N+1)/2; i++)
		{
			rQ[i].real = z[2*i].real;
			rQ[i].imag = z[2*i].imag;
		}

		for (i = 0; i < (N-1)/2; i++)
		{
			rP[i].real = z[(2*i)+1].real;
			rP[i].imag = z[(2*i)+1].imag;
		}

		// Include the conjugates as well : rQ = [rQ; conj(rQ)]; rP = [rP; conj(rP)];
		for (i = (N+1)/2; i < N+1; i++)
		{
			rQ[i].real = rQ[i - ((N+1)/2)].real;
			rQ[i].imag = -rQ[i - ((N+1)/2)].imag;
		}

		for (i = (N-1)/2; i < N-1; i++)
		{
			rP[i].real = rP[i - ((N-1)/2)].real;
			rP[i].imag = -rP[i - ((N-1)/2)].imag;
		}

		// Form the polynomials P and Q, note that these should be real
		Q = calloc((N+2), sizeof(double));
		P = calloc(N, sizeof(double));
		
		if (Q == NULL || P == NULL)
		{
			printf("Could not allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		mypoly(rQ, N+1, Q);
		//printf("\nTestOdd1!\n");
		mypoly(rP, N-1, P);
		//printf("\nTestOdd2!\n");
		free(z);
		z = NULL;

		/* Form the sum and difference filters by including known roots at z = 1 and
		 *  z = -1 
		 */	

		if (p % 2 == 1)
		{
			// Odd order: z = +1 and z = -1 are roots of the difference filter, P1(z)
			tmp = calloc(3, sizeof(double));
			P1 = calloc((N+2), sizeof(double));

			if (tmp == NULL || P1 == NULL)
			{	
				printf("Could not allocate memory!\n");
				exit(EXIT_FAILURE);
			}

			tmp[0] = 1.0; tmp[1] = 0.0; tmp[2] = -1.0;
			//printf("\nTestOdd3!\n");
			myconv_real(P, tmp, N, 3, P1);
			//printf("\nTestOdd4!\n");
			Q1 = Q;
			free(tmp);
			tmp = NULL;
		}
		else
		{
			// Even order: z = -1 is a root of the sum filter, Q1(z) and z = 1 is a
			// root of the difference filter, P1(z)
			tmp = calloc(2, sizeof(double));
			P1 = calloc(N+1, sizeof(double));
			Q1 = calloc(N+3, sizeof(double));

			if (Q1 == NULL || P1 == NULL || tmp == NULL)
			{	
				printf("Could not allocate memory!\n");
				exit(EXIT_FAILURE);
			}

			tmp[0] = 1.0; tmp[1] = -1.0;
			//printf("\nTestOdd5!\n");
			myconv_real(P, tmp, N, 2, P1);
			//printf("\nTestOdd6!\n");
			tmp[1] = 1.0;
		   	myconv_real(Q, tmp, N+2, 2, Q1);
			//printf("\nTestOdd7!\n");
			free(tmp);
			tmp = NULL;
			free(P);
			P = NULL;
			free(Q);
			Q = NULL;
		}

		// Prediction polynomial is formed by averaging P1 and Q1

		for (i = 0; i < N+1; i++)
		{
			a[i] = 0.5 * (P1[i] + Q1[i]);
			//printf("a[%d] = %.20lf\n", i, a[i]);
		}

		free(P1);
		P1 = NULL;
		free(Q1);
		Q1 = NULL;

		//a[N] = 0.0;    // The last coefficient is zero and is not returned
	}
	else
	{
		rQ = calloc(N, sizeof(COMPLEX));
		rP = calloc(N, sizeof(COMPLEX));

		if (rQ == NULL || rP == NULL)
		{
			printf("Could not allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		// Separate the zeros to those belonging to P and Q
		for (i = 0; i < N/2; i++)
		{
			rQ[i].real = z[2*i].real;
			rQ[i].imag = z[2*i].imag;
			rP[i].real = z[(2*i)+1].real;
			rP[i].imag = z[(2*i)+1].imag;
		}

		// Include the conjugates as well : rQ = [rQ; conj(rQ)]; rP = [rP; conj(rP)];
		for (i = N/2; i < N; i++)
		{
			rQ[i].real = rQ[i - N/2].real;
			rQ[i].imag = -rQ[i - N/2].imag;
			rP[i].real = rP[i - N/2].real;
			rP[i].imag = -rP[i - N/2].imag;
		}

		// Form the polynomials P and Q, note that these should be real
		Q = calloc((N+1), sizeof(double));
		P = calloc((N+1), sizeof(double));

		if (Q == NULL || P == NULL)
		{
			printf("Could not allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		//printf("before poly");
		mypoly(rQ, N, Q);
		//printf("\nTest1Even!\n");
		mypoly(rP, N, P);
		//for (i = 0; i < N; i++)
		//	printf("rP[%d] = %.20lf + %.20lfi rQ[%d] = %.20lf + %.20lfi\n", i, rP[i].real, rP[i].imag, i, rQ[i].real, rQ[i].imag);
		//printf("\nTest2Even!\n");
		free(z);
		z = NULL;

		/* Form the sum and difference filters by including known roots at z = 1 and
		 *  z = -1 
		 */	

		if (p % 2 == 1)
		{
			// Odd order: z = +1 and z = -1 are roots of the difference filter, P1(z)
			tmp = calloc(3, sizeof(double));
			P1 = calloc(N+3, sizeof(double));

			if (tmp == NULL || P1 == NULL)
			{	
				printf("Could not allocate memory!\n");
				exit(EXIT_FAILURE);
			}

			tmp[0] = 1.0; tmp[1] = 0.0; tmp[2] = -1.0;
			//printf("\nTest3Even!\n");
			myconv_real(P, tmp, N+1, 3, P1);
			Q1 = Q;
			//printf("\nTest4Even!\n");
			//for (i = 0; i < N+1; i++)
			//	printf("Q1[%d] = %.20lf P1[%d] = %.20lf\n", i, Q1[i], i, P1[i]);
			free(tmp);
			tmp = NULL;
			free(P);
			P = NULL;
		}
		else
		{
			// Even order: z = -1 is a root of the sum filter, Q1(z) and z = 1 is a
			// root of the difference filter, P1(z)
			tmp = calloc(2, sizeof(double));
			P1 = calloc(N+2, sizeof(double));
			Q1 = calloc(N+2., sizeof(double));

			if (Q1 == NULL || P1 == NULL || tmp == NULL)
			{
				printf("Could not allocate memory!\n");
				exit(EXIT_FAILURE);
			}

			tmp[0] = 1.0; tmp[1] = -1.0;
			myconv_real(P, tmp, N+1, 2, P1);
			//printf("\nTest5Even!\n");
			tmp[1] = 1.0;
		   	myconv_real(Q, tmp, N+1, 2, Q1);
			//printf("\nTest6Even!\n");
			//for (i = 0; i < N+1; i++)
			//	printf("Q1[%d] = %.20lf P1[%d] = %.20lf\n", i, Q1[i], i, P1[i]);

			free(tmp);
			tmp = NULL;
			free(P);
			P = NULL;
			free(Q);
			Q = NULL;
		}

		// Prediction polynomial is formed by averaging P1 and Q1

		for (i = 0; i < N+1; i++)
		{
			a[i] = 0.5 * (P1[i] + Q1[i]);
			//printf("a[%d] = %.20lf\n", i, a[i]);
		}

		free(P1);
		P1 = NULL;
		free(Q1);
		Q1 = NULL;

		//printf("\nTest8!\n");
		//a[N] = 0.0;    // The last coefficient is zero and is not returned
	}
}








/* Function : ac2poly
 *
 * Purpose : converts autocorrelation sequence to prediction polynomial.
 *
 * Input Arguments : double *ac   <- the autocorrelation sequence vector
 *                   double *poly <- the prediction polynomial (output)
 * 		     int N        <- the size of the autocorrelation vector
 *
 * Output Arguments: int <- 0 : Normally completed
 *                         -1 : Abnormally completed
 *                         -2 : Unstable polynomial
 *
 * References:
 * 	Kay, S.M. Modern Spectral Estimation. Englewood Cliffs, NJ: Prentice-Hall, 1988
 *
 */
int ac2poly(double *ac, double *poly, int order)
{
	int state;

	state = levdur(ac, poly, order-1, -1);

	if (state == -1)
		printf("ac2poly() : Warning : Terminated abnormally!\n");
	if (state == -2)
		printf("ac2poly() : Warning : Unstable polynomial!\n");

	poly[0] = 1.00000000000000000;

	return state;
}







//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//												//
//												//
//				W I N D O W   F U N C T I O N S		 			//
//												//
//												//
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////







/* Function : hanning
 *
 * Purpose: returns a N-point Hanning window, according to a parameter about symmetry
 *
 * Input Arguments : int L    <- the length of the hanning window
 * 		     int type <- the type of the window (0 == symmetric, 1 == periodic)
 *
 * Output Arguments : double *hann <- the requested hanning window
 */
double *hanning(int L, int type)
{
	int i;
	double *hann = calloc(L, sizeof(double));

	if (hann == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	if (type == 0)
	{
		for (i = 0; i < L; i++)
			hann[i] = 0.5 * (1 - cos(2*PI*(i+1)/(L+1)));
	}
	else if (type == 1)
	{
		for (i = 0; i < L; i++)
			hann[i] = 0.5 * (1 - cos(2*PI*i/L));

		hann[0] = 0.0;
	}
	else
	{
		printf("The requested hanning window type is invalid!\n");
		exit(EXIT_FAILURE);
	}

	return hann;
}





/* Function : hanning_rec
 *
 * Purpose : returns a N-point Hanning window, according to a parameter about symmetry
 *
 * In this implementation, we are following the recursive algorithm that is shown below:
 * 		
 * 	cos(2*PI*k/N) = 2cos(2*PI*(k-1)/N)cos(2*PI/N) - cos(2*PI*(k-2)/N)
 *
 *
 * Input Arguments : int L    <- the length of the hanning window
 * 		     int type <- the type of the window (0 == symmetric, 1 == periodic)
 *
 * Output Arguments : double *hann <- the requested hanning window
 *
 */
double *hanning_rec(int L, int type)
{
	int i;
	double t0 = 1.0, t1 = 0.0, t2 = 0.0, t3 = 0.0, S = 0.0;
	double *hann = calloc(L, sizeof(double));

	if (hann == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	if (type == 0)
	{
		t1 = cos(2*PI/(L+1));
		S = t1;
		t2 = 2 * t1 * S - t0;
		hann[0] = 0.5 * (1 - t1);
		hann[1] = 0.5 * (1 - t2);

		for (i = 2; i < L; i++)
		{
			t3 = 2 * t2 * S - t1;
			hann[i] = 0.5 * (1 - t3);
			t1 = t2;
			t2 = t3;
		}
	}
	else if (type == 1)
	{
		t1 = cos(2*PI/L);
		S = t1;
		t2 = 2 * t1 * S - t0;
		hann[0] = 0.0;
		hann[1] = 0.5 * (1 - t1);
		hann[2] = 0.5 * (1 - t2);

		for (i = 3; i < L; i++)
		{
			t3 = 2 * t2 * S - t1;
			hann[i] = 0.5 * (1 - t3);
			t1 = t2;
			t2 = t3;
		}
	}
	else
	{
		printf("hanning_rec() : Hanning type is not recognized!\n");
		exit(EXIT_FAILURE);
	}

	return hann;
}







//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//												//
//												//
//    M A T R I X  M A N I P U L A T I O N  &  L I N E A R  S Y S T E M S  F U N C T I O N S	//
//												//
//												//
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////









/* Function : transpose
 *
 * Purpose : calculates the transpose matrix of an input 2D matrix
 *
 * Input Arguments : double **A <- the 2D input matrix
 * 		     int M      <- the number of rows of the 2D matrix
 * 		     int N      <- the number of cols of the 2D matrix
 *
 * Output Arguments: double ** <- the transpose matrix
 */
double **transpose(double **A, int M, int N)
{
	int i, j;
	double **A_tr = calloc(N, sizeof(double *));

	if (A_tr == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	for(i = 0; i < N; i++)
	{
		if ((A_tr[i] = calloc(M, sizeof(double))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}	
	}
	
	for(i = 0; i < M; i++)
		for(j = 0; j < N; j++)
       			A_tr[j][i] = A[i][j];

	return A_tr;
}







/* Function : matrmult
 *
 * Purpose : multiplies two 2D matrices, A, B, and results in C
 *
 * Input Arguments : double **A <- the first 2D matrix A with size [M,N]
 * 		     int M      <- the number of rows of the first 2D matrix
 * 		     int N      <- the number of cols of the first 2D matrix
 * 		     double **B <- the second 2D matrix B with size [N,K]
 * 		     int K      <- the number of cols of the second 2D matrix
 *
 * (Output Arguments:) 
 * 		     double **C <- the output 2D matrix, C = A*B, with size [M,K]
 */
void matrmult(double **A, int M, int N, double **B, int K, double **C)
{
	int k,l,j;
	
	for(k = 0; k < M; k++) 
	{
		for(l = 0; l < K; l++) 
		{
			C[k][l] = 0.0;
			
			for(j = 1; j < N; j++)
				C[k][l] += (A[k][j] * B[j][l]);
		}
	}
}







/* Function : forwardSubstitution
 *
 * Purpose : the first part of the the Gaussian elimination algorithm
 *
 * Input Arguments : double **a <- the 2D "augmented" matrix of equations (A | b)
 * 		     int n      <- the number of rows of the 2D matrix
 *
 * Output Arguments: -
 */
void forwardSubstitution(double **a, int n) 
{
	int i, j, k, max;
	double t;
	
	for (i = 0; i < n; ++i) 
	{
		max = i;
		for (j = i + 1; j < n; ++j)
			if (fabs(a[j][i]) > fabs(a[max][i]))
				max = j;

		for (j = 0; j < n + 1; ++j) 
		{
			t = a[max][j];
			a[max][j] = a[i][j];
			a[i][j] = t;
		}

		/* Singular matrix? */
	        if (fabs(a[i][i]) < DBL_EPSILON)
		{
			printf("Matrix is near singular!\n");
			exit(EXIT_FAILURE);
		}


		for (j = n; j >= i; --j)
			for (k = i + 1; k < n; ++k)
				a[k][j] -= a[k][i]/a[i][i] * a[i][j];

/*		for (k = 0; k < n; ++k) {
			for (j = 0; j < n + 1; ++j)
				printf("%.2f\t", a[k][j]);
			printf("\n");
		}*/
	}


	//if (fabs(a[n+1][n+1]) < DBL_EPSILON)
	//{
	//	printf("Matrix is near singular!\n");
	//	exit(EXIT_FAILURE);
	//}
}






/* Function : reverseElimination
 *
 * Purpose : the second part of the the Gaussian elimination algorithm
 *
 * Input Arguments : double **a <- the 2D "augmented" matrix of equations (A | b)
 * 		     int n      <- the number of rows of the 2D matrix
 *
 * Output Arguments: -
 */
void reverseElimination(double **a, int n) 
{
	int i, j;

	for (i = n - 1; i >= 0; --i) 
	{
		a[i][n] = a[i][n] / a[i][i];
		a[i][i] = 1.0;
		
		for (j = i - 1; j >= 0; --j) 
		{
			a[j][n] -= a[j][i] * a[i][n];
			a[j][i] = 0.0;
		}
	}
}







/* Function : gauss
 *
 * Purpose : solves a linear system of equations Ax = b, using the Gaussian elimination method
 *
 * Input Arguments : double **a <- the 2D "augmented" matrix of equations (A | b)
 * 		     int n      <- the number of rows of the 2D matrix
 *
 * Output Arguments: double * <- the solution x of the equation set Ax=b
 */
double *gauss(double **a, int n) 
{
	int i, j;
	double *x = calloc(n, sizeof(double));

	if (x == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	forwardSubstitution(a,n);
	reverseElimination(a,n);

	//for (i = 0; i < n; ++i) 
	//{
	//	for (j = 0; j < n + 1; ++j)
	//		printf("%.20lf\t", a[i][j]);
	//	printf("\n");
	//}

	for (i = 0; i < n; i++)
		x[i] = a[i][n];

	return x;
}








/* Function : gauss_jordan
 *
 * Purpose : linear equation solution by Gauss-Jordan elimination. a[1..n][1..n] 
 * is the input matrix. b[1..n][1..n] is input containing the m right hand side 
 * vectors. On output, a is replaced by its matrix inverese and b is replaced
 * by the correspoinding set of solution vectors
 *
 * Input Arguments : double **a <- the input matrix A, in AX = B.
 * 		     int n      <- the number of rows of the A matrix
 *
 *(Output Arguments: )
 * 		     double **b <- the m right hand side vectors (input)
 * 		                   the set of solution vectors (output)
 * 		     int m      <- the number of columns of B matrix
 *
 * Taken from : Numerical Recipes in C, 1992.
 */
void gauss_jordan(double **a, int n, double **b, int m)
{
	int *indxc, *indxr, *ipiv;
	int i, irow, icol, j, k, l, ll;
	double big, dum, pivinv, temp;

	if ((indxc = calloc(n, sizeof(int))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	if ((indxr = calloc(n, sizeof(int))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}
	
	if ((ipiv = calloc(n, sizeof(int))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	indxc = indxc - 1;
	indxr = indxr - 1;
	ipiv = ipiv - 1;

	for (j = 1; j <= n; j++)
		ipiv[j] = 0;
	for (i = 1; i <= n; i++)
	{
		big = 0.0;
	
		for (j = 1; j <= n; j++)
		{
			if (ipiv[j] != 1)
			{
				for (k = 1; k <= n; k++)
				{
					if (ipiv[k] == 0)
					{
						if (fabs(a[j][k]) >= big)
						{
							big = fabs(a[j][k]);
							irow = j;
							icol = k;
						}
					}
				}
			}
		}
		++(ipiv[icol]);

		if (irow != icol)
		{
			for (l = 1; l <= n; l++)
				SWAP(a[irow][l], a[icol][l]);
			for (l = 1; l <= m; l++)
				SWAP(b[irow][l], b[icol][l]);
		}

		indxr[i] = irow;
		indxc[i] = icol;

		if (a[icol][icol] == 0.0)
		{
			printf("gauss_jordan() : Singular matrix!\n");
			exit(EXIT_FAILURE);
		}

		pivinv = 1.0/a[icol][icol];

		for (l = 1; l <= n; l++)
			a[icol][l] *= pivinv;
		for (l = 1; l <= m; l++)
			b[icol][l] *= pivinv;

		for (ll = 1; ll <= n; ll++)
			if (ll != icol)
			{
				dum = a[l][icol];
				a[ll][icol] = 0.0;
				for (l = 1; l <= n; l++)
					a[ll][l] -= a[icol][l] * dum;
				for (l = 1; l <= m; l++)
					b[ll][l] -= b[icol][l] * dum;
			}
	}

	for (l = n; l >= 1; l--)
	{
		if (indxr[l] != indxc[l])
			for (k = 1; k <= n; k++)
				SWAP(a[k][indxr[l]], a[k][indxc[l]]);
	}

	ipiv = ipiv + 1;
	indxr = indxr + 1;
	indxc = indxc + 1;
	free(ipiv);
	free(indxr);
	free(indxc);
}










/* Function : repmat
 *
 * Purpose : creates a large matrix B consisting of an M-by-N tiling of copies of A
 *
 * Input Arguments : double **A <- the input matrix
 * 		     int rowA   <- the number of rows of A
 * 		     int colA   <- the number of columns of A
 * 		     int M      <- the number of rows of B
 * 		     int N      <- the number of cols of B
 *
 * Output Arguments: double **B <- the output matrix B
 *
 */
double **repmat(double **A, int rowA, int colA, int M, int N)
{
	int i, j;
	int rowB = rowA*M;
	int colB = colA*N;
	double **B = calloc(rowB, sizeof(double *));

	if (B == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	for(i = 0; i < rowB; i++)
	{
		if ((B[i] = calloc(colB, sizeof(double))) == NULL)
		{
			printf("Cannot allocate memory!\n");
			exit(EXIT_FAILURE);
		}
	}

	for(i = 0; i < rowB; i++)
		for(j = 0; j < colB; j++)
		{
			if(rowA == 1)
				B[i][j] = A[0][j%colA];
			else if(colA == 1)
				B[i][j] = A[0][i%rowA];
			else
				B[i][j] = A[i%rowA][j%colA];
		}

	return B;	
}








/* Function : ludcmp
 *
 * Purpose : given a matrix a[1...n][1..n], this routine replaces it by the LU
 *          decomposition of a rowwise permutation of itself. a and n are input.
 *          a is output, arranged appropriately. indx[1...n] is an output vector
 *          that records the row permutation effected by the partial pivoting.
 *          d is output as +-1 depending on whether the number of row interchanges
 *          was even or odd, respectively. This routine is used in combination with
 *          lubksb to solve linear equations or invert a matrix
 *
 * Input Arguments : double **a <- the matrix A
 *                   int n      <- the size of matrix A (nxn)
 *
 * (Output Arguments: )
 *                   int *indx <- petrmutation vector
 * 		     double *d <- 1 for even row interchanges
 * 		                  -1 for odd interchanges
 *
 * Taken from : Numerical Recipes in C (1992)
 */
void ludcmp(double **a, int n, int *indx, double *d)
{
	int i, imax, j, k;
	double big, dum, sum, temp;
	double *vv;

	if ((vv = calloc(n, sizeof(double))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}
	vv = vv - 1;

	*d = 1.0;

	for (i = 1; i <= n; i++)
	{
		big = 0.0;

		for (j = 1; j <= n; j++)
			if ((temp = fabs(a[i][j])) > big)
				big = temp;

		if (big == 0.0)
		{
			printf("Singular matrix in routine ludcmp");
			exit(EXIT_FAILURE);
		}

		vv[i] = 1.0/big;
	}

	for (j = 1; j <= n; j++)
	{
		for (i = 1; i < j; i++)
		{
			sum = a[i][j];

			for (k = 1; k < i; k++)
				sum -= a[i][k]*a[k][j];

			a[i][j] = sum;
		}

		big = 0.0;

		for (i = j; i <= n; i++)
		{
			sum = a[i][j];

			for (k = 1; k < j; k++)
				sum -= a[i][k] * a[k][j];

			a[i][j] = sum;

			if ((dum = vv[i]*fabs(sum)) >= big)
			{
				big = dum;
				imax = i;
			}
		}

		if (j != imax)
		{
			for (k = 1; k <= n; k++)
			{
				dum = a[imax][k];
				a[imax][k] = a[j][k];
				a[j][k] = dum;
			}

			*d = -(*d);
			vv[imax] = vv[j];
		}

		indx[j] = imax;

		if (a[j][j] == 0.0)
			a[j][j] = TINY;

		if (j != n)
		{
			dum = 1.0/(a[j][j]);

			for (i = j + 1; i <= n; i++)
				a[i][j] *= dum;
		}
	}

	vv = vv + 1;
	free(vv);
}





/* Function : lubksb
 *
 * Purpose : solves the set of linear equations AX = B. Here, a[1...n][1...n]
 *          input, not as the matrix A but rahter as its LU decomposition,
 *          determined by the routine ludcmp. indx[1..n] is input as the
 *          permutation vector returned by ludcmp. b[1...n] is input as the
 *          right hand side vector B, and returns with the solution vector X.
 *          a, n, and indx are not modified by this routine and can be left in
 *          place for successive calls with different right-hand sides b. This
 *          routine takes into account the possibility that b will begin with
 *          many zero elements, so it is efficient for use in matrix inversion
 *
 * Input Arguments : double **a <- the matrix A
 *                   int n      <- the size of matrix A (nxn)
 *                   int *indx  <- petrmutation vector
 * (Output Arguments: )
 * 		     double *b <- the vector B (input)
 * 		                  the solution of the system AX=B (output)
 *
 * Taken from : Numerical Recipes in C (1992)
 */
void lubksb(double **a, int n, int *indx, double *b)
{
	int i, ii = 0, ip, j;
	double sum;

	for (i = 1; i <= n ; i++)
	{
		ip = indx[i];
		sum = b[ip];
		b[ip]=b[i];
		
		if (ii)
			for (j = ii; j <= i-1; j++)
				sum -= a[i][j] * b[j];
		else if (sum)
			ii = i;
		
		b[i] = sum;
	}
	for (i = n; i >= 1; i--)
	{
		sum=b[i];

		for (j = i+1; j <= n; j++)
			sum -= a[i][j] * b[j];

		b[i] = sum/a[i][i];
	}
}





/* Function : LUdecomp
 *
 * Purpose : LU decomposition for solution of a linear system of equations AX = B
 *
 * Input Arguments : double **a <- the matrix A
 *                  int n      <- the size of matrix A (nxn)
 *
 * (Output Arguments: )
 * 		    double *b <- the matrix B (as input)
 * 		                 the solution of the system AX = B (as output)
 */
void LUdecomp(double **a, int n, double *b)
{
	int i, j;
	double d;
	int *indx;

	if ((indx = calloc(n, sizeof(int))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < n; i++)
		*(a + i) = *(a + i) - 1;

	a = a - 1;
	b = b - 1;
	indx = indx - 1;
#ifdef DEBUG
	printf("Before ludcmp\n");
#endif
	ludcmp(a, n, indx, &d);
#ifdef DEBUG
	printf("Before lubksb\n");
#endif
	lubksb(a, n, indx, b);
#ifdef DEBUG
	printf("After lubksb\n");
#endif

	a = a + 1;
	for (i = 0; i < n; i++)
		*(a + i) = *(a + i) + 1;

	b = b + 1;
	indx = indx + 1;

	free(indx);
}









//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//												//
//												//
//			I N T E R P O L A T I O N   F U N C T I O N S	 			//
//												//
//												//
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////








/* Function : polint
 *
 * Purpose : given arrays xa[1...n] and ya[1...n] and given a value x, this
 *           routine returns a value y, and an error estimate dy. If P(x) is
 *           the polynomial of degree N-1 such that P(xai) = yai, i = 1...n,
 *           then the returned value y = P(x)
 *
 * Input Arguments : double *xa <- the x-values of the tabulated function
 *                   double *ya <- the y-values of the tabulated function
 *                   int n      <- the size of the above arrays
 *                   double x   <- the value x
 * (Output Arguments: )
 *                   double *y  <- the value y = P(x)
 *                   double *dy <- the error estimate dy
 *
 * Taken from : Numerical Recipes in C, 1992
 */
void polint(double *xa, double *ya, int n, double x, double *y, double *dy)
{
	int i, m, ns = 1;
	double den, dif, dift, ho, hp, w;
	double *c, *d;

	dif = fabs(x-xa[1]);

	if ((c = calloc(n, sizeof(double))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	if ((d = calloc(n, sizeof(double))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	c = c - 1;
	d = d - 1;

	for (i = 1; i <= n; i++)
	{
		if ((dift = fabs(x-xa[i])) < dif)
		{
			ns = i;
			dif = dift;
		}

		c[i] = ya[i];
		d[i] = ya[i];
	}

	*y = ya[ns--];

	for (m = 1; m < n; m++)
	{
		for (i = 1; i <= n-m; i++)
		{
			ho = xa[i] - x;
			hp = xa[i+m] - x;
			w = c[i+1] - d[i];

			if ((den = ho - hp) == 0.0)
			{
				printf("polint() : Error in routine polint!\n");
				exit(EXIT_FAILURE);
			}

			den = w/den;
			d[i] = hp*den;
			c[i] = ho*den;
		}

		*y += (*dy=(2*ns < (n-m) ? c[ns+1]:d[ns--]));
	}

	c = c+1;
	d = d+1;
	free(c);
	free(d);
}









/* Function : locate
 *
 * Purpose : given an array xx[1..n] and given a value x, returns a value
 *           j such that x is betweem xx[j] and xx[j+1]. xx must be monotonic
 *           either increasing or decreasing. j = 0 or j = n is returned to 
 *           indicate that x is out of range
 *
 * Input Arguments : double *xx       <- the array to be searched
 *                   unsigned long n  <- the size of the array
 *                   double x         <- the value to search its index
 *(Output Arguments: )
 *                   unsigned long *j <- the returned index
 *
 * Taken from Numerical Recipes in C, 1992.
 */
void locate(double *xx, unsigned long n, double x, unsigned long *j)
{
	unsigned long ju, jm, jl;
	int ascnd;

	jl = 0;
	ju = n+1;
	ascnd = (xx[n] >= xx[1]);
	
	while (ju - jl > 1)
	{
		jm = (ju + jl) >> 1;

		if (x >= xx[jm] == ascnd)
			jl = jm;
		else
			ju = jm;
	}

	if (x == xx[1])
		*j = n-1;
	else
		*j = jl;
}









/* Function : linear_interp
 *
 * Purpose : performs linear interpolation to a vector set xi, according to
 *           the tabulated function (xa, ya)
 *
 * Input Arguments : double *xa <- the x-values of the tabulated function
 *                   double *ya <- the y-values of the tabulated function
 *                   int N      <- the size of the above vectors
 *                   double *xi <- the xi-points vector to perform interpolation with
 *                   int M      <- the size of the above array xi
 * (Output Arguments: )
 *                   double *y  <- the interpolated points y(xi)
 */
void linear_interp(double *xa, double *ya, int N, double *xi, int M, double *y)
{
	int i, j;
	double Xi_minus_1 = 0.0;
	double Xi_plus_1 = 0.0;
	double Yi_minus_1 = 0.0;
	double Yi_plus_1 = 0.0;
	double tmp = 0.0;


	for (j = 0; j < M; j++)
	{
		tmp = xi[j];

		// if the xi value is outside the region, then the minimum or maximum function value is returned
		if (tmp > xa[N-1])
		{
			y[j] = ya[N-1];
			continue;
		}

		if (tmp < xa[0])
		{
			y[j] = ya[0];
			continue;
		}

		// Algorithm for locating Xi-1, Xi+1 (simple, not optimal)
		for (i = 0; i < N-1; i++)
		{
			if (tmp >= xa[i] && tmp <= xa[i+1])
			{
				if (tmp == xa[i])
				{	
					y[j] = ya[i];
					break;
				}

				if (tmp == xa[i+1])
				{
					y[j] = ya[i+1];
					break;
				}

				Xi_minus_1 = xa[i];
				Xi_plus_1 = xa[i+1];
				Yi_minus_1 = ya[i];
				Yi_plus_1 = ya[i+1];
#ifdef DEBUG
				printf("xi-1 = %lf, xi+1 = %lf, yi-1 = %lf, yi+1 = %lf\n", Xi_minus_1, Xi_plus_1, Yi_minus_1, Yi_plus_1);
#endif
				// Performing linear interpolation
				y[j] = Yi_minus_1 + (tmp - Xi_minus_1)*(Yi_plus_1 - Yi_minus_1)/(Xi_plus_1 - Xi_minus_1);
#ifdef DEBUG
				printf("y[%d] = %lf\n", j, y[j]);
#endif
				break;
			}
		}
	}
}







/* Function : nearest_neighbour_interp
 *
 * Purpose : performs nearest neighbour interpolation to a vector set xi,
 *           according to the tabulated function (xa, ya), where ya is
 *           complex
 *
 * Input Arguments : double *xa  <- the x-values of the tabulated function
 *                   COMPLEX *ya <- the complex y-values of the tabulated function
 *                   int N       <- the size of the above arrays
 *                   double *xi  <- the xi-points vector to perform interpolation with
 *                   int M       <- the size of the xi-array
 *(Output Arguments: ) 
 *                   COMPLEX *y  <- the interpolated complex values
 */
void nearest_neighbour_interp(double *xa, COMPLEX *ya, int N, double *xi, int M, COMPLEX *y)
{
	int i, j;
	double tmp = 0.0;
	double d1 = 0.0, d2 = 0.0;

	for (j = 0; j < M; j++)
	{
		tmp = xi[j];

		if (tmp > xa[N-1])
		{
			y[j].real = ya[N-1].real;
			y[j].imag = ya[N-1].imag;
			continue;
		}

		if (tmp < xa[0])
		{
			y[j].real = ya[0].real;
			y[j].imag = ya[0].imag;
			continue;
		}

		for (i = 0; i < N-1; i++)
		{
			if (tmp >= xa[i] && tmp <= xa[i+1])
			{
				if (tmp == xa[i])
				{
					y[j].real = ya[i].real;
					y[j].imag = ya[i].imag;
					break;
				}
				if (tmp == xa[i+1])
				{
					y[j].real = ya[i+1].real;
					y[j].imag = ya[i+1].imag;
					break;
				}
				
				d1 = fabs(tmp - xa[i]);
				d2 = fabs(xa[i+1] - tmp);
	
				if (d1 < d2)
				{
					y[j].real = ya[i].real;
					y[j].imag = ya[i].imag;
					break;
				}
				else
				{
					y[j].real = ya[i+1].real;
					y[j].imag = ya[i+1].imag;
					break;
				}	
			}
		}
	}
}








/* Function : spline
 *
 * Purpose : given arrays x[1...n] and y[1...n] containing a tabulated function
 *           i.e. yi = f(xi), with x1 < x2 < ... < xN, and given values yp1 and
 *           ypn for the first derivatives of the interpolating function at points
 *           1 and n, respectively, this routine returns an array y2[1...n] that
 *           contains the second derivatives of the interpolating function at the
 *           tabulated points xi. If yp1 and/or ypn are equal to 1x10^30 or larger,
 *	     the routine is signaled to set the corresponding boundary condition
 *	     for a natural spline, with zero second derivative on that boundary
 *
 * Input Arguments : double *x  <- the x-values of the tabulated function
 *                   double *y  <- the y-values of the tabulated function
 *                   int n      <- the size of the above arrays
 *                   double yp1 <- the first derivative value at point 1
 *                   double ypn <- the fisrt derivative value at point n
 * (Output Arguments: )
 *                   double *y2 <- the second derivatives of the interpolating function
 *                                 at the tabulated points xi
 *
 * Taken from : Numerical Recipes in C, 1992.
 */
void spline(double *x, double *y, int n, double yp1, double ypn, double *y2)
{
	int i, k;
	double p, qn, sig, un, *u;

	if ((u = calloc(n-1, sizeof(double))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	u = u - 1;

	if (yp1 > 0.99e30)
		y2[1] = u[1] = 0.0;
	else
	{
		y2[1] = -0.5;
		u[1] = (3.0/(x[2] - x[1]))*((y[2] - y[1])/(x[2] - x[1]) - yp1);
	}

	for (i = 2; i <= n-1; i++)
	{
		sig = (x[i] - x[i-1])/(x[i+1] - x[i-1]);
		p = sig*y2[i-1] + 2.0;
		y2[i] = (sig - 1.0)/p;
		u[i] = (y[i+1] - y[i])/(x[i+1] - x[i]) - (y[i] - y[i-1])/(x[i] - x[i-1]);
		u[i] = (6.0*u[i]/(x[i+1] - x[i-1]) - sig*u[i-1])/p;
	}

	if (ypn > 0.99e30)
		qn = un = 0.0;
	else
	{
		qn = 0.5;
		un = (3.0/(x[n] - x[n-1])) * (ypn - (y[n] - y[n-1])/(x[n] - x[n-1]));
	}

	y2[n] = (un - qn*u[n-1])/(qn*y2[n-1] + 1.0);

	for (k = n-1; k >= 1; k--)
		y2[k] = y2[k]*y2[k+1] + u[k];

	u = u + 1;
	free(u);
}








/* Function : splint
 *
 * Purpose : given the arrays xa[1...n] and ya[1...n], which tabulate a function
 *           (with xai's in order), and given the array y2a[1...n], which is the
 *           output from spline above, and given an value of x, this routine
 *           returns a cubic-spline interpolated value y
 *
 * Input Arguments : double *xa  <- the x-values of the tabulated function
 *                   double *ya  <- the y-values of the tabulated function
 *                   double *y2a <- the second derivative array produced by spline()
 *                   int n       <- the size of the above arrays
 *                   double x    <- the value to be interpolated
 * (Output Arguments: )
 *                   double *y   <- the cubic-spline interpolated value
 *
 * Taken from : Numerical Recipes in C, 1992.
 */
void splint(double *xa, double *ya, double *y2a, int n, double x, double *y)
{
	int klo, khi, k;
	double h, b, a;

	klo = 1;
	khi = n;

	while (khi - klo > 1)
	{
		k = (khi + klo) >> 1;
		if (xa[k] > x)
			khi = k;
		else
			klo = k;
	}

	h = xa[khi] - xa[klo];

	if (h == 0.0)
	{
		printf("Splint() : Bad xa input to routine splint()!\n");
		exit(EXIT_FAILURE);
	}

	a = (xa[khi] - x)/h;
	b = (x - xa[klo])/h;
	//printf("a = %lf, b = %lf, ya = %lf, ya = %lf, y2a = %lf, y2a = %lf\n", a, b, ya[klo], ya[khi], ya[klo], y2a[klo], y2a[khi]);

	*y = a*ya[klo] + b*ya[khi] + ((a*a*a-a)*y2a[klo] + (b*b*b-b)*y2a[khi])*(h*h)/6.0;
	//printf("y = %lf\n", *y);
}









/* Function : spline_interp
 *
 * Purpose : performs spline interpolation to a vector set xi, according to
 *           the tabulated function (xa, ya)
 *
 * Input Arguments : double *xa <- the x-values of the tabulated function
 *                   double *ya <- the y-values of the tabulated function
 *                   int N      <- the size of the above vectors
 *                   double *xi <- the xi-points vector to perform interpolation with
 *                   int M      <- the size of the above array xi
 * (Output Arguments: )
 *                   double *y  <- the interpolated points y(xi)
 */
void spline_interp(double *xa, double *ya, int N, double *xi, int M, double *y)
{
	int i;
	double tmp = 0.0;
	double *y2 = calloc(N, sizeof(double));

	// Addressing shift for the algorithm
	y2 = y2 - 1;
	xa = xa - 1;
	ya = ya - 1;

	spline(xa, ya, N, 0.0, 0.0, y2); // sure about the values?

	for (i = 0; i < M; i++)
	{
		tmp = xi[i];

		if (tmp < xa[1])
		{
			y[i] = ya[1];
			continue;
		}
		else if (tmp > xa[N])
		{
			y[i] = ya[N];
			continue;
		}
		else
			splint(xa, ya, y2, N, tmp, &y[i]);
	}

	// Addressing shift back to normal
	xa = xa + 1;
	ya = ya + 1;
	y2 = y2 + 1;
	free(y2);
}









/* Function : polynomial_interp
 *
 * Purpose : performs polynomial interpolation to a vector set xi, according to
 *           the tabulated function (xa, ya)
 *
 * Input Arguments : double *xa <- the x-values of the tabulated function
 *                   double *ya <- the y-values of the tabulated function
 *                   int N      <- the size of the above vectors
 *                   double *xi <- the xi-points vector to perform interpolation with
 *                   int M      <- the size of the above array xi
 * (Output Arguments: )
 *                   double *y  <- the interpolated points y(xi)
 */
void polynomial_interp(double *xa, double *ya, int N, double *xi, int M, double *y)
{
	double tmp;
	double err;
	int i, j;

	for (j = 0; j < M; j++)
	{
		tmp = xi[j];

		if (tmp > xa[N-1])
		{
			y[j] = ya[N-1];
			continue;
		}

		if (tmp < xa[0])
		{
			y[j] = ya[0];
			continue;
		}

		// Algorithm for locating Xi-1, Xi+1 (simple, not optimal, O(N) complexity)
		for (i = 0; i < N-1; i++)
		{
			if (tmp >= xa[i] && tmp <= xa[i+1])
			{
				if (tmp == xa[i])
				{	
					y[j] = ya[i];
					break;
				}

				if (tmp == xa[i+1])
				{
					y[j] = ya[i+1];
					break;
				}

				// Performing polynomial interpolation
				polint(xa, ya, N, tmp, &y[j], &err);
#ifdef DEBUG
				printf("y[%d] = %lf\n", j, y[j]);
#endif
				break;
			}
		}
	}
}









void pchip_interp(double *xa, double *ya, int N, double *xi, int M, double *y)
{
	// Piecewise Cubic Hermite Interpolating Polynomial (PCHIP) Interpolation
	//
	// Not implemented
}










/* Function : interpolation
 *
 * Purpose : wrapper function for several different interpolation schemes
 *
 * Input Arguments : double *xa <- the x-values of the tabulated function
 *                   void *ya   <- the y-values of the tabulated function
 *                   int N      <- the size of the above arrays
 *                   double *xi <- the values to perform interpolation with
 *                   int M      <- the size of the above array
 *                   char *meth <- the interpolation method
 *(Output Arguments: )
 *                   void *y    <- the interpolated values
 */
void interpolation(double *xa, void *ya, int N, double *xi, int M, char *meth, void *y)
{
	double *dptr1 = NULL;
	COMPLEX *cptr1 = NULL;
	double *dptr2 = NULL;
	COMPLEX *cptr2 = NULL;

	if (meth != NULL)
	{
		if (strcmp(meth, "linear") == 0)
		{
			//printf("Linear interpolation selected.\n");
			dptr1 = (double *)ya;
			dptr2 = (double *)y;
			linear_interp(xa, dptr1, N, xi, M, dptr2);
		}
		else if (strcmp(meth, "NN") == 0)
		{
			//printf("Nearest Neighbour interpolation selected.\n");
			cptr1 = (COMPLEX *)ya;
			cptr2 = (COMPLEX *)y;
			nearest_neighbour_interp(xa, cptr1, N, xi, M, cptr2);
		}
		//else if (strcmp(meth, "cubic") == 0)
		//{
		//	dptr1 = (double *)ya;
		//	dptr2 = (double *)y;
		//	pchip_interp(xa, dptr1, N, xi, M, dptr2);
		//}
		else if (strcmp(meth, "spline") == 0)
		{
			//printf("Spline interpolation selected.\n");
			dptr1 = (double *)ya;
			dptr2 = (double *)y;
			spline_interp(xa, dptr1, N, xi, M, dptr2);
			//printf("Spline interpolation succeeded.\n");
		}
		else if (strcmp(meth, "polynomial") == 0)
		{
			dptr1 = (double *)ya;
			dptr2 = (double *)y;
			polynomial_interp(xa, dptr1, N, xi, M, dptr2);
		}
		else
		{
			printf("Interpolation method \"%s\" is not recognized!\n", meth);
			exit(EXIT_FAILURE);
		}
	}
	else
	{
		dptr1 = (double *)ya;
		dptr2 = (double *)y;
		linear_interp(xa, dptr1, N, xi, M, dptr2);
	}
}









/* Function : SM_analysis_frame(Xf, F, Fs, W, options)
*
*	Computes the amplitudes and the phase of a set of sinusoids (Sinusoidal
*	Model, SM) from a single frame of speech.
*
*	Input Arguments:
*	Xf:	[Nx1]  the speech frame (N samples)
*	N:	[1x1]  the length of the signal Xf
*	F:	[Lx1]  the frequencies of the sinusoids
*	L:	[1x1]  the number of the frequencies of the sinusoids
*	Fs:	[1x1]  sampling rate
*	W:	[Nx1]  the window that will be applied to the data (default = hanning(N))
*   options:    [string] estimation type:
*               'complex':  solve the complex linear system formulation for
*                           the least-square-error solution
*               'cos-sin': solve the two cosine-sine linear system formulation 
*                           for the least-square-error solution (default)
*
*	Output Arguments:
*	AMP:	[Lx1]   the amplitudes of the sinusoids
*	PH:	[Lx1]   the phases of the sinusoids
*	Yf:	[Nx1]	synthesized signal (note: a window is applied to the
*	                signal)
*   	SNR:    [1x1]   the Signal-To-Noise ratio of the analysis
*   	A:      [Lx1]   complex amplitudes   
*
*/
void SM_analysis_frame(double *Xf, int N, double *F, int L, double Fs, double *Wu, char *options, double **wfreq, int wfreq_M, int wfreq_N, double *AMP, double *PH, double *Yf, double *SNR, COMPLEX *A)
{
	int i, j, LL = 0;
	double *wXf = NULL, *n = NULL, *FF = NULL, **CC = NULL, **SS = NULL, **Cw = NULL;
        double **Sw = NULL, **CM = NULL, **SM = NULL, *cos_A = NULL, *sin_A = NULL, **W_ = NULL;
	double **temp1 = NULL, **temp2 = NULL, **t1 = NULL, **t2 = NULL, *temp3 = NULL, *temp4 = NULL, **pptr = NULL, *ptr = NULL;
	double tmp1 = 0.0, tmp2 = 0.0;
	COMPLEX *ctemp = NULL;
	
	if (F[0] > 0.0)
	{
		tmp1 = meanVal(Xf, N);

		for (i = 0; i < N; i++)
			Xf[i] = Xf[i] - tmp1; // remove DC
	}

	if ((wXf = calloc(N, sizeof(double))) == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	for (i = 0; i < N; i++)
	{
		wXf[i] = Xf[i]*Wu[i];  // window signal
#ifdef SM_DEBUG
		printf("wXf[%d] = %lf\n", i, wXf[i]);
#endif
	}

	n = calloc(N, sizeof(double));

	for (i = 0; i < N; i++)
		n[i] = i+1 - (N+1)/2; // time indices (assuming that tc = (N+1/2) )

	// DEFAULT is "cos-sin", so we skip the rest methods...
	
	if (strncmp(options, "complex", 7) == 0)
	{
		printf("This is method is not yet implemented!\n");
		exit(EXIT_FAILURE);
	}
	else if (strncmp(options, "cos-sin", 7) == 0)
	{			
		CC = calloc(N, sizeof(double *));
		SS = calloc(N, sizeof(double *));

		for (i = 0; i < N; i++)
		{
			CC[i] = calloc(L, sizeof(double));
			SS[i] = calloc(L, sizeof(double));
		}

		for (i = 0; i < N; i++)
		{
			for (j = 0; j < L; j++)
			{
				CC[i][j] = cos((2*PI/Fs)*n[i]*F[j]);
				SS[i][j] = sin((2*PI/Fs)*n[i]*F[j]);
			}
		}


		if (wfreq != NULL)
		{	
			temp1 = wfreq;
			ptr = Wu;
			pptr = &ptr;

			t1 = repmat(temp1, wfreq_M, wfreq_N, N, 1);
			t2 = repmat(pptr, N, 1, 1, L);

			for (i = 0; i < N; i++)
				for (j = 0; j < L; j++)
					W_[i][j] = t1[i][j] * t2[i][j];

			for (i = 0; i < N; i++)
			{
				free(t1[i]);
				t1[i] = NULL;
				free(t2[i]);
				t2[i] = NULL;
			}
			
			free(t1);
			t1 = NULL;
			free(t2);
			t2 = NULL;
		}
		else
		{
			ptr = Wu;
			pptr = &ptr;
        		W_ = repmat(pptr, N, 1, 1, L);
		}

#ifdef SM_DEBUG
		for (i = 0; i < N; i++)
			for (j = 0; j < L; j++)
				printf("W_[%d][%d] = %lf\n", i, j, W_[i][j]);
#endif
		
		Cw = calloc(N, sizeof(double *));
		Sw = calloc(N, sizeof(double *));

		for (i = 0; i < N; i++)
		{
			Cw[i] = calloc(L, sizeof(double));
			Sw[i] = calloc(L, sizeof(double));
		}

		for (i = 0; i < N; i++)
			for (j = 0; j < L; j++)
			{
				Cw[i][j] = W_[i][j] * CC[i][j];
				Sw[i][j] = W_[i][j] * SS[i][j];
			}

		for (i = 0; i < N; i++)
		{
			free(W_[i]);
			W_[i] = NULL;
		}

		free(W_); // good.
		W_ = NULL;

#ifdef SM_DEBUG
		for (i = 0; i < N; i++)
			for (j = 0; j < L; j++)
				printf("Cw[%d][%d] = %lf\tSw[%d][%d] = %lf\n", i, j, Cw[i][j], i, j, Sw[i][j]);
#endif

		CM = calloc(L, sizeof(double *));
		SM = calloc(L, sizeof(double *));

		for (i = 0; i < L; i++)
		{
			CM[i] = calloc(L, sizeof(double));
			SM[i] = calloc(L, sizeof(double));
		}

		temp1 = transpose(Cw, N, L);
		temp2 = transpose(Sw, N, L);

		matrmult(temp1, L, N, Cw, L, CM);
		matrmult(temp2, L, N, Sw, L, SM);

		cos_A = calloc(L, sizeof(double));
		sin_A = calloc(L, sizeof(double));

		for (i = 0; i < L; i++)
		{
			for (j = 0; j < N; j++)
			{
				cos_A[i] += temp1[i][j] * wXf[j];
				sin_A[i] += temp2[i][j] * wXf[j];
			}
#ifdef SM_DEBUG
			printf("cos_A[%d] = %.20lf\tsin_A[%d] = %.20lf\n", i, cos_A[i], i, sin_A[i]);
#endif
		}

		//printf("Before linear system solution...\n");
		LUdecomp(CM, L, cos_A); // solve linear systems
		LUdecomp(SM, L, sin_A); // solve linear systems
		//printf("After linear system solution...\n");

#ifdef SM_DEBUG
		for (i = 0; i < L; i++)
		{
			printf("cos_A[%d] = %.20lf\n", i, cos_A[i]);
			printf("sin_A[%d] = %.20lf\n", i, sin_A[i]);
		}
#endif

		//cos_A = CM \ (Cw'*wXf); // solve linear systems
	        //sin_A = SM \ (Sw'*wXf); // solve linear systems
	    	
		ctemp = calloc(L, sizeof(COMPLEX));

		for (i = 0; i < L; i++)
		{
			ctemp[i].real = cos_A[i];
			ctemp[i].imag = -sin_A[i];
		}

		if (A != NULL)
			memcpy(A, ctemp, L * sizeof(COMPLEX));

	    	if (Yf != NULL)
		{
			for(i = 0; i < N; i++)
				for(j = 0; j < L; j++)
					Yf[i] += CC[i][j]*cos_A[j] + SS[i][j]*sin_A[j]; // synthesize if requested
		}
	}
 	// DEFAULT is "sin-cos", so we skip these...
	else if (strncmp(options, "fast-harm", 9) == 0)
	{
		printf("This is method is not yet implemented!\n");
		exit(EXIT_FAILURE);
	}
	else
	{
		printf("Option %s is not recognized", options);
		exit(EXIT_FAILURE);
	}

	temp3 = CabsVal(ctemp, L); // harmonic amplitudes
	temp4 = angle(ctemp, L);    // harmonic phases

	for (i = 0; i < L; i++)
	{
		AMP[i] = temp3[i];
		PH[i] = temp4[i];
	}

	free(ctemp);
	ctemp = NULL;
	free(temp3);
	temp3 = NULL;
	free(temp4);
	temp4 = NULL;
	
	if (SNR != NULL && Yf != NULL)
	{
		temp3 = calloc(N, sizeof(double));

		for (i = 0; i < N; i++)
			temp3[i] = wXf[i] - Wu[i] * Yf[i];

		*SNR = 20*log10(stdev(wXf, N)/stdev(temp3, N));
		free(temp3);
	}


	//
	// Start free()-ing vectors and matrices
	//
	
#ifdef SM_DEBUG
	printf("Just before the frees...\n");
#endif
	free(wXf);
	wXf = NULL;
	free(n);
	n = NULL;
	free(cos_A);
	cos_A = NULL;
	free(sin_A);
	sin_A = NULL;

	for (i = 0; i < L; i++)
	{
		free(temp1[i]);
		temp1[i] = NULL;
		free(temp2[i]);
		temp2[i] = NULL;
		free(CM[i]);
		CM[i] = NULL;
		free(SM[i]);
		SM[i] = NULL;
	}

	free(temp1);
	temp1 = NULL;
	free(temp2);
	temp2 = NULL;
	free(CM);
	CM = NULL;
	free(SM);
	SM = NULL;

	for (i = 0; i < N; i++)
	{
		free(Cw[i]);
		Cw[i] = NULL;
		free(Sw[i]);
		Sw[i] = NULL;
		free(CC[i]);
		CC[i] = NULL;
		free(SS[i]);
		SS[i] = NULL;
	}

	free(Cw);
	Cw = NULL;
	free(Sw);
	Sw = NULL;
	free(CC);
	CC = NULL;
	free(SS);
	SS = NULL;
}






/* Function : box_muller 
 *
 * Purpose : implements the Polar form of the Box-Muller Transformation
 *
 * Input Arguments : double m <- the mean value of the distribution
 * 		     double s <- the variance of the distribution
 *
 * Output Arguments : double <- a random number taken from the distibution
 *
 * (c) Copyright 1994, Everett F. Carter Jr.
 * Permission is granted by the author to use
 * this software for any application provided this
 * copyright notice is preserved.
 *
*/
double box_muller(double m, double s)	/* normal random variate generator */
{				        /* mean m, standard deviation s */
	double x1, x2, w, y1;
	static double y2;
	static int use_last = 0;
	
	if (use_last)		        /* use value from previous call */
	{
		y1 = y2;
		use_last = 0;
	}
	else
	{
		do {
			x1 = 2.0 * dr250() - 1.0;
			x2 = 2.0 * dr250() - 1.0;
			w = x1 * x1 + x2 * x2;
		} while ( w >= 1.0 );

		w = sqrt( (-2.0 * log( w ) ) / w );
		y1 = x1 * w;
		y2 = x2 * w;
		use_last = 1;
	}

	return( m + y1 * s );
}




/* Function : norm_noise
 *
 * Purpose: generates a vector 1xN with random number generated by a
 * normal distribution of mean "mean" and variance "var"
 *
 * Input Arguments : double mean <- the mean value of the distribution
 * 		     double var  <- the variance of the distribution
 * 		     int N       <- the size of the vector
 *
 * Output Argmuments : double * <- a vector 1xN containing the generated random numbers
 */
double *norm_noise(double mean, double var, int N)
{
	int i;
	double *res = calloc(N, sizeof(double));

	if (res == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}
	if (N <= 0)
	{
		printf("norm_noise() : N must be positive!\n");
		exit(EXIT_FAILURE);
	}

	//r250_init(time(NULL));

	for (i = 0; i < N; i++)
		res[i] = box_muller(mean, var);

	return res;
}





/* Function : unif_noise
 *
 * Purpose : generates a vector 1xN with random number generated by a
 * uniform distribution in the interval [0,1]
 *
 * Input Arguments : int N <- the size of the vector
 *
 * Output Argmuments : double * <- a vector 1xN containing the generated random numbers
 *
 */
double *unif_noise(int N)
{
	int i;
	double *res = calloc(N, sizeof(double));

	if (res == NULL)
	{
		printf("Cannot allocate memory!\n");
		exit(EXIT_FAILURE);
	}
	if (N <= 0)
	{
		printf("norm_noise() : N must be positive!\n");
		exit(EXIT_FAILURE);
	}

	//r250_init(time(NULL));

	for (i = 0; i < N; i++)
		res[i] = dr250();

	return res;
}






/* Function : pit_sync_win
 *
 * Purpose : creates a window that is suitable for pitch-synchronous analysis/synthesis.
 * 
 * Input Arguments: double T1   <- left-side pitch period
 * 		    double T2   <- right-side pitch period
 * 		    int wintype <- type of window: [ left-side right-side ]
 * 				   0 : [ hanning(T1) hanning(T2) ]
 *
 * (Output Arguments: )
 *                  double *W <- resulting window
 *
*/

void pit_sync_win(double T1, double T2, int wintype, double *W)
{
	int i, j, L;
	double *W1, *W2;

	L = T1 + T2 + 1;
	T1 = T1 + 1;
	T2 = T2 + 1;

	switch (wintype)
	{
		case 0: {
				W1 = hanning_rec(2*T1, 1); //periodic
				W2 = hanning_rec(2*T2, 1); //periodic

				for (j = 0; j < T1; j++)
					W[j] = W1[j];
				for (i = T2+1; i < 2*T2; i++) // C-consistent : T2 + 2 - 1
					W[j++] = W2[i];

				break;
			}
		default: {
				 printf("Window type <%d> is not recognized!\n", wintype);
			 }
	}

	free(W1);
	W1 = NULL;
	free(W2);
	W2 = NULL;
}






/* Function : SM_synth_frame
*
* Purpose : sinusoidal Model synthesis. It synthesizes one frame of speech signal 
*   	    from the parameters of the sinusoidal model.
*
* Input Arguments: int N       <- the length of the (output) speech frame is N samples
*		   double Fs   <- sampling rate
*		   double *AMP <- the amplitude values array
*		   int L       <- the number of sinusoids and phases
*		   double *PH  <- the phase values array
*		   double *F   <- the sampled frequencies
*   		   int *I      <- synthesizes a subset of these sinusoids; only the
*               	    	  sinusoids with indices in I.
*		   int J       <- the length of I vector
*   		   double Tc   <- the center of the synthesis window (in samples, could be non integer)
*   		   int K       <- oversample ratio (gives K samples for every sample of speech) K = 1; by default
*
* (Output Arguments: )
*		   Yf <- the synthesized speech frame
*   		   nn <- the time indices of the synthesized samples
*
*/
void SM_synth_frame(int N, double Fs, double *AMP, int L, double *PH, double *F, int *I, int J, double Tc, double K, double *Yf, int *nn)
{
	double *tAMP, *tPH, *tF, *temp, tmp;
	int i, k, m;
	int t;

	if ((tAMP = calloc(J, sizeof(double))) == NULL)
	{
		printf("Could not allocate memory!\n");
		exit(EXIT_FAILURE);
	}
	
	if ((tPH = calloc(J, sizeof(double))) == NULL)
	{
		printf("Could not allocate memory!\n");
		exit(EXIT_FAILURE);
	}
	
	if ((tF = calloc(J, sizeof(double))) == NULL)
	{
		printf("Could not allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	if (Tc == -1)
		Tc = (N+1)/2.0;

	if (J > 0 && I != NULL)
	{
		for (i = 0; i < J; i++)
		{
			t = I[i];
			tAMP[i] = AMP[t];
			tPH[i] = PH[t]; 
			tF[i] = F[t];
		}
	}

	for (i = 0; i < (int)(K)*N; i++)
	{
		nn[i] = ((i+1) - Tc*K)/K;
		// nn = ([1:K*N]'-Tc*K)/K;
	}
	
	if ((temp = calloc((K*N), sizeof(double))) == NULL)
	{
		printf("Could not allocate memory!\n");
		exit(EXIT_FAILURE);
	}

	// OPTIMIZATION ??? 
	for (i = 0; i < L; i++)
	{
		for (k = 0; k < K*N; k++)
			temp[k] = AMP[i]*cos((2*PI*F[i]/Fs)*nn[k] + PH[i]);

		for (m = 0; m < K*N; m++)
			Yf[m] = Yf[m] + temp[m];
	}

	free(temp);
	temp = NULL;
	free(tAMP);
	tAMP = NULL;
	free(tPH);
	tPH = NULL;
	free(tF);
	tF = NULL;
}






/* Function : ran
 *
 * Purpose: generates a random number generated by a uniform distribution over [0, 1)
 *
 * Input Arguments : long *iseed <- the seed to initialize the generator
 *
 * Output Argmuments : double <- the random number generated
 */
double ran(long *iseed)
{
    *iseed = N_a * (*iseed % N_q) - N_r * (*iseed / N_q);          // update seed

    if (*iseed < 0)                              // wrap to positive values
           *iseed += N_m;

    return (double) *iseed / (double) N_m;
}





/* Function : gran
 *
 * Purpose: generates a random number generated by a normal distribution 
 *          of mean "m" and variance "s"
 *
 * Input Arguments : double m    <- the mean value of the distribution
 * 		     double s    <- the variance of the distribution
 * 		     long *iseed <- the seed to initialize the generator
 *
 * Output Argmuments : double <- a random number generated by a normal distribution 
 */
double gran(double m, double s, long *iseed)
{
       double v = 0;
       int i;

       for (i = 0; i < 12; i++)          // sum 12 uniform random numbers
              v += ran(iseed);

       return s * (v - 6) + m;           // adjust mean and variance
}




/* Function : gnoise
 *
 * Purpose: generates a vector 1xN with random number generated by a
 * normal distribution of mean "m" and variance "s"
 *
 * Input Arguments : double m    <- the mean value of the distribution
 * 		     double s    <- the variance of the distribution
 * 		     int N       <- the size of the vector
 * 		     long *iseed <- the seed to initialize the generator
 *
 * Output Argmuments : double * <- a vector 1xN containing the generated random numbers
 */
double *gnoise(double m, double s, int N, long *iseed)
{
	int i;
	double *r = calloc(N, sizeof(double));

	for (i = 0; i < N; i++)
		r[i] = gran(m, s, iseed);

	return r;
}







/* Function: lfwave(lf, T0, N, U)
*
* Purpose : construct a frame of a periodic LF waveform.
*
* Input Arguments : double *lf <- LF source parameters + intrinsic representation
*		    double T0  <- glottal period
*                   int N      <- number of samples to synthesize = 2*N+1
*
* (Output Arguments: ) 
* 		    double *U  <- LF waveform (2*N+1)
*/

void lfwave(double *lf, double T0, int N, double *U)
{
	double K;
	double *Uf, *Ut, *temp; //Ut was U, at first
	double Nu;
	double Tc;
	int i, j;
	source_lfintra_t lfmod;

	// CONVERT *lf to source_lfintra_t lfmod !!!! //
	lfmod.src.Oq = lf[0];
	lfmod.src.am = lf[1];
	lfmod.src.Qa = lf[2];
	lfmod.src.Av = lf[3];
	lfmod.src.ac = lf[4];
	lfmod.alphatilde = lf[5];
	lfmod.betatilde = lf[6];

	/*printf("Oq = %lf\n", lfmod.src.Oq);
	printf("am = %lf\n", lfmod.src.am);
	printf("Qa = %lf\n", lfmod.src.Qa);
	printf("Av = %lf\n", lfmod.src.Av);
	printf("ac = %lf\n", lfmod.src.ac);
	printf("alpha~ = %lf\n", lfmod.alphatilde);
	printf("beta~ = %lf\n", lfmod.betatilde);*/

	if (round(T0) == T0)
	{
		/*
		 *   when T0 is an integer
		 */
		if ((Uf = calloc((2*T0 + 1), sizeof(double))) == NULL)
		{
			printf("Could not allocate memory!\n");
			exit(EXIT_FAILURE);
		}
    		LFwave(Uf, 2*(int)T0+1 , T0, T0-2, T0-2, &lfmod); // synthesize two periods (+1) 
						                  // -1 to all vars to be comp. with C ...
		//for (i = 0; i < 2*T0 + 1; i++)
		//	printf("%.10lf\n", Uf[i]);

    		K = 2 * round(((double)N)/T0) + 1;
    		Nu = (K+1) * T0 + 1;
    		Ut = calloc(Nu, sizeof(double));

		for (i = 0; i < K; i++)
			for (j = i*T0; j <= (2 + i)*T0; j++)
				Ut[j] = Ut[j] + Uf[j - i*(int)T0];

    		Tc = (Nu + 1)/2;
		free(Uf);
		Uf = NULL;

		if ((temp = calloc((2*N + 1), sizeof(double))) == NULL)
		{
			printf("Could not allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		for (i = -N; i <= N; i++)
			temp[i + N] = Ut[(int)Tc-1 + i];

		if ((Ut = calloc((2*N + 1), sizeof(double))) == NULL)
		{
			printf("Could not allocate memory!\n");
			exit(EXIT_FAILURE);
		}

		for (i = 0; i < 2*N + 1; i++)
			Ut[i] = temp[i]; // take a symmetric region of +-2*T0 around 3*T0+1

		free(temp); 
		temp = NULL;

    		// replace NaN with zeros (!!!???)
		for (i = 0; i < 2*N + 1; i++)
			if (isnan(Ut[i]) != 0)
			{
				//printf("Brhka NaN! U = %.15lfn", Ut[i]);
				Ut[i] = 0.00000000;
				//printf("New U = %.15lf\n", Ut[i]);
			}

    		//I = find(Ut ~= Ut);
    		//if length(I)>0
        		//*warning('bad LF source (*d NaNs)',length(I));
        	//Ut(I) = 0;
    		//end
		for (i = 0; i < 2*N + 1; i++)
			U[i] = Ut[i];

		free(Ut);
		Ut = NULL;
	}
	else
	{
    		printf("Function does not accept fractional pitch values!\n"); 
		exit(EXIT_FAILURE);
	}
}














//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//												//
//												//
//				S P T K    F U N C T I O N S		 			//
//												//
//												//
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////






/*
  ---------------------------------------------------------------  
            Speech Signal Processing Toolkit (SPTK)

                      SPTK Working Group                           
                                                                   
                  Department of Computer Science                   
                  Nagoya Institute of Technology                   
                               and                                 
   Interdisciplinary Graduate School of Science and Engineering    
                  Tokyo Institute of Technology                    
                                                                   
                     Copyright (c) 1984-2007                       
                       All Rights Reserved.                        
                                                                   
  Permission is hereby granted, free of charge, to use and         
  distribute this software and its documentation without           
  restriction, including without limitation the rights to use,     
  copy, modify, merge, publish, distribute, sublicense, and/or     
  sell copies of this work, and to permit persons to whom this     
  work is furnished to do so, subject to the following conditions: 
                                                                   
    1. The source code must retain the above copyright notice,     
       this list of conditions and the following disclaimer.       
                                                                   
    2. Any modifications to the source code must be clearly        
       marked as such.                                             
                                                                   
    3. Redistributions in binary form must reproduce the above     
       copyright notice, this list of conditions and the           
       following disclaimer in the documentation and/or other      
       materials provided with the distribution.  Otherwise, one   
       must contact the SPTK working group.                        
                                                                   
  NAGOYA INSTITUTE OF TECHNOLOGY, TOKYO INSTITUTE OF TECHNOLOGY,   
  SPTK WORKING GROUP, AND THE CONTRIBUTORS TO THIS WORK DISCLAIM   
  ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL       
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   
  SHALL NAGOYA INSTITUTE OF TECHNOLOGY, TOKYO INSTITUTE OF         
  TECHNOLOGY, SPTK WORKING GROUP, NOR THE CONTRIBUTORS BE LIABLE   
  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY        
  DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,  
  WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTUOUS   
  ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR          
  PERFORMANCE OF THIS SOFTWARE.                                    
                                                                   
  ---------------------------------------------------------------  
*/

/********************************************************
   $Id: _fft.c,v 1.8 2007/09/30 16:20:30 heigazen Exp $               
       NAME:               
                fft - fast fourier transform    
       SYNOPSIS:               
                int   fft(x, y, m);         
                     
                double   x[];   real part      
                double   y[];   imaginary part      
                int      m;     data size      
   
                return : success = 0
                         fault   = -1
       Naohiro Isshiki          Dec.1995    modified   
********************************************************/


//
// Modifications : 1) added a free(_sintbl); at the end
//
//                  are undone....
//
double *_sintbl=0;
int maxfftsize=0;

static int checkm (const int m)
{
   int k;

   for (k=4; k<=m; k<<=1) {
      if (k==m)
         return(0);
   }
   fprintf(stderr, "fft : m must be a integer of power of 2!\n");

   return(-1);
}

int fft(double *x, double *y, const int m)
{
   int j, lmx, li;
   double *xp, *yp;
   double *sinp,*cosp;
   int lf, lix,tblsize;
   int mv2, mm1;
   double t1, t2;
   double  arg;
   //int checkm(const int);

   /**************
   * RADIX-2 FFT *
   **************/

   if (checkm(m))
      return(-1);

   /***********************
   * SIN table generation *
   ***********************/

   if ((_sintbl==0) || (maxfftsize<m)) {
      tblsize=m-m/4+1;
      arg=PI/m*2;
      if (_sintbl!=0)
         free(_sintbl);
      _sintbl = sinp = dgetmem(tblsize);
      *sinp++ = 0;
      for (j=1; j<tblsize; j++)
         *sinp++ = sin(arg*(double)j);
      _sintbl[m/2] = 0;
      maxfftsize = m;
   }

   lf = maxfftsize / m;
   lmx = m;

   for(;;) {
      lix = lmx;  
      lmx /= 2;
      if (lmx <= 1) break;
      sinp = _sintbl;
      cosp = _sintbl + maxfftsize/4;
      for (j=0; j<lmx; j++) {
         xp = &x[j];
         yp = &y[j];
         for (li=lix; li<=m ; li+=lix) {
            t1 = *(xp) - *(xp + lmx);
            t2 = *(yp) - *(yp + lmx);
            *(xp) += *(xp + lmx);
            *(yp) += *(yp + lmx);
            *(xp + lmx) = *cosp * t1 + *sinp * t2;
            *(yp + lmx) = *cosp * t2 - *sinp * t1;
            xp += lix;
            yp += lix;
         }
         sinp += lf;
         cosp += lf;
      }
      lf += lf;
   }

   xp = x;
   yp = y;
   for (li=m/2; li--; xp+=2,yp+=2) {
      t1 = *(xp) - *(xp + 1);
      t2 = *(yp) - *(yp + 1);
      *(xp) += *(xp + 1);
      *(yp) += *(yp + 1);
      *(xp + 1) = t1;
      *(yp + 1) = t2;
   }
   
   /***************
   * bit reversal *
   ***************/
   j = 0;
   xp = x;
   yp = y;
   mv2 = m / 2;
   mm1 = m - 1; 
   for (lmx=0; lmx<mm1; lmx++) {
      if ((li=lmx-j)<0) {
         t1 = *(xp);
         t2 = *(yp);
         *(xp) = *(xp + li);
         *(yp) = *(yp + li);
         *(xp + li) = t1;
         *(yp + li) = t2;
      }
      li = mv2;
      while (li<=j) {
         j -= li;
         li /= 2;
      }
      j += li;
      xp = x + j;
      yp = y + j;
   }

   return(0);
}









/********************************************************
 $Id: _fftr.c,v 1.8 2007/09/12 08:37:12 heigazen Exp $                     
 
 NAME:                     
        fftr - Fast Fourier Transform for Double sequence      
 SYNOPSIS:                                             
        int   fftr(x, y, m)            
                     
        double  x[];   real part of data      
        double  y[];   working area         
        int     m;     number of data(radix 2)      
                Naohiro Isshiki    Dec.1995   modified
********************************************************/
int fftr (double *x, double *y, const int m)
{
   int i, j;
   double *xp, *yp, *xq;
   double *yq;
   int mv2, n, tblsize;
   double xt, yt, *sinp, *cosp;
   double arg;
   
   mv2 = m / 2;

   /* separate even and odd  */
   xq = xp = x;
   yp = y;
   for (i=mv2; --i>=0; ) {
      *xp++ = *xq++;
      *yp++ = *xq++;
   }

   if (fft(x, y, mv2)==-1)        /* m / 2 point fft */
      return(-1);


   /***********************
   * SIN table generation *
   ***********************/

   if ((_sintbl==0) || (maxfftsize<m)) {
      tblsize=m-m/4+1;
      arg=PI/m*2;
      if (_sintbl!=0)
         free(_sintbl);
      _sintbl = sinp = dgetmem(tblsize);
      *sinp++ = 0;
      for (j=1; j<tblsize; j++)
         *sinp++ = sin( arg * (double)j);
      _sintbl[m/2] = 0;
      maxfftsize = m;
   }

   n = maxfftsize / m;
   sinp = _sintbl;
   cosp = _sintbl + maxfftsize/4;

   xp = x;
   yp = y;
   xq = xp + m;
   yq = yp + m;
   *(xp + mv2) = *xp - *yp;
   *xp = *xp + *yp;
   *(yp + mv2) = *yp = 0;
   
   for (i=mv2,j=mv2-2; --i ; j-=2) {
      ++xp;
      ++yp;
      sinp += n;
      cosp += n;
      yt = *yp + *(yp + j);
      xt = *xp - *(xp + j);
      *(--xq) = (*xp + *(xp + j) + *cosp * yt - *sinp * xt) * 0.5;
      *(--yq) = (*(yp + j) - *yp + *sinp * yt + *cosp * xt) * 0.5;
   }

   xp = x + 1;
   yp = y + 1;
   xq = x + m;
   yq = y + m;
   
   for (i=mv2; --i; ) {
      *xp++ =   *(--xq); 
      *yp++ = -(*(--yq));
   }
   
   return(0);
}






/****************************************************************

    $Id: _levdur.c,v 1.11 2007/09/30 07:34:15 heigazen Exp $

    Solve an Autocorrelation Normal Equation
    Using Levinson-Durbin Method

       int levdur(r, a, m, eps);

       double  *r    : autocorrelation sequence
       double  *a    : LP coefficients
       int     m     : order of LPC
       double  eps   : singular check (eps(if -1., 1.0e-6 is assumed))

       return  value :  0  -> normally completed
                        -1 -> abnormally completed
                        -2 -> unstable LPC

******************************************************************/


// Modifications : 1) static double *c = NULL; -> double *c = NULL;
//                 2) static int size;         -> int size;
//                 3) if (m>size) ...          -> into comments
//                 4) a free(c) is added to the end of the function (unnecessary if c is static)
//
//                 are undone...
//
int levdur (double *r, double *a, const int m, double eps)
{
   int l, k, flag=0;
   double rmd, mue;
   static double *c=NULL;
   //double *c=NULL;
   static int size;
   //int size;

   if (c==NULL) {
      c = dgetmem(m+1);
      size = m;
   }

   if (m>size) {
      free(c);
      c = dgetmem(m+1);
      size = m;
   }

   if (eps<0.0) eps = 1.0e-6;
   rmd=r[0];
   if ( (((rmd<0.0)?-rmd:rmd)<=eps) || isnan(rmd) ) return(-1);
   a[0] = 0.0;

   for (l=1; l<=m; l++) {
      mue = -r[l];
      for (k=1; k<l; k++)
         mue -= c[k] * r[l - k];
      mue = mue / rmd;

      for (k=1; k<l; k++)
         a[k] = c[k] + mue * c[l - k];
      a[l] = mue;

      rmd = (1.0 - mue * mue) * rmd;
      if ( (((rmd<0.0)?-rmd:rmd)<=eps) || isnan(rmd) ) return(-1);
      if (((mue<0.0) ? -mue : mue) >= 1.0) flag = -2;

      for (k=0; k<=l; k++) c[k] = a[k];
   }
   a[0] = sqrt(rmd);
   
   //free(c);
   return(flag);
}







/*****************************************************************
*  $Id: _ifft.c,v 1.11 2007/09/12 08:37:12 heigazen Exp $         *
*  NAME:                                                         *
*      ifft - Inverse Fast Fourier Transform                     *   
*  SYNOPSIS:                                                     *
*      int   ifft(x, y, m)                                       *
*                                                                *
*      real   x[];   real part                                   *
*      real   y[];   imaginary part                              *
*      int    m;     size of FFT                                 *
*****************************************************************/
int ifft (double *x, double *y, const int m)
{
   int i;

   if (fft(y, x, m)==-1)
      return(-1);

   for (i=m; --i>=0; ++x, ++y) {
      *x /= m; 
      *y /= m;
   }

   return(0);
}







/***************************************************************
    $Id: ifftr.c,v 1.5 2007/09/12 08:37:14 heigazen Exp $

    Inverse Fast Fourier Transform for Real Sequence

    int ifftr(x, y, l)

    double *x : real part of data
    double *y : working area
    int     l : number of data(radix 2)

***************************************************************/
int ifftr(double *x, double *y, const int l)
{
   int i;
   double *xp, *yp;

   fftr(x, y, l);

   xp = x; yp = y; 
   i = l;
   while (i--) {
      *xp++ /= l;
      *yp++ /= -l;
   }

   return 0;
}












/********************************************************************
    $Id: getmem.c,v 1.9 2007/09/30 16:20:25 heigazen Exp $

    Memory Allocation Functions

    double *dgetmem(leng)

    int leng : data length

    char *getmem(leng, size)

    size_t leng : data length
    size_t size : size of data type

**********************************************************************/
char *getmem (const size_t leng, const size_t size)
{
   char *p = NULL;

   if ((p = (char *)calloc(leng, size)) == NULL){
      fprintf(stderr, "Cannot allocate memory!\n");
      exit(3);
   }
   return (p);
}

double *dgetmem (const int leng)
{
   return ( (double *)getmem((size_t)leng, sizeof(double)) );
}








/********************************************************
    $Id: fillz.c,v 1.6 2007/09/18 03:52:41 heigazen Exp $

    Fill Data with Zero

    fillz(ptr, size, nitem)

    void   *ptr  : intput data
    size_t size  : size of data type
    int    nitem : data length

*********************************************************/
void fillz (void *ptr, const size_t size, const int nitem)
{
   long n;
   char *p = ptr;
    
   n = size * nitem;
   while(n--)
      *p++ = '\0';
}






/********************************************************
    $Id: movem.c,v 1.6 2007/09/18 03:52:41 heigazen Exp $

    Data Transfer Function

    movem(a, b, size, nitem)

    void   *a    : intput data
    void   *b    : output data
    size_t size  : size of data type
    int    nitem : data length

*********************************************************/
void movem (void *a, void *b, const size_t size, const int nitem)
{
   long i;
   char *c = a;
   char *d = b;

   i = size * nitem;
   if (c > d)
      while (i--) *d++ = *c++;
   else {
      c+=i;  d+=i;
      while (i--) *--d = *--c;
   }
}






/****************************************************************

    $Id: _lsp2lpc.c,v 1.8 2007/09/12 08:37:19 heigazen Exp $

    Transformation LSP to LPC

        void lsp2lpc(lsp, a, m)

        double  *lsp : LSP
        double  *a   : LPC
        int      m   : order of LPC

*****************************************************************/
void lsp2lpc (double *lsp, double *a, const int m)
{
   int i, k, mh1, mh2, flag_odd;
   double xx, xf, xff;
   static double *f = NULL, *p, *q, *a0, *a1, *a2, *b0, *b1, *b2;
   static int   size;

   flag_odd = 0;
   if (m%2==0)
      mh1 = mh2 = m / 2;
   else {
      mh1 = (m + 1) / 2;
      mh2 = (m - 1) / 2;
      flag_odd = 1;
   }

   if (f==NULL) {
      f = dgetmem(5*m+6);
      p  = f  + m;
      q  = p  + mh1;
      a0 = q  + mh2;
      a1 = a0 + (mh1+1);
      a2 = a1 + (mh1+1);
      b0 = a2 + (mh1+1);
      b1 = b0 + (mh2+1);
      b2 = b1 + (mh2+1);
      size = m;
   }
   if (m>size) {
      free(f);
      f = dgetmem(5*m+6);
      p  = f  + m;
      q  = p  + mh1;
      a0 = q  + mh2;
      a1 = a0 + (mh1+1);
      a2 = a1 + (mh1+1);
      b0 = a2 + (mh1+1);
      b1 = b0 + (mh2+1);
      b2 = b1 + (mh2+1);
      size = m;
   }

   movem(lsp, f, sizeof(*lsp), m);

   fillz(a0, sizeof(*a0), mh1+1);
   fillz(b0, sizeof(*b0), mh2+1);
   fillz(a1, sizeof(*a1), mh1+1);
   fillz(b1, sizeof(*b1), mh2+1);
   fillz(a2, sizeof(*a2), mh1+1);
   fillz(b2, sizeof(*b2), mh2+1);

   /* lsp filter parameters */
   for (i=k=0; i<mh1; i++,k+=2)
      p[i] = -2.0 * cos(PI2 * f[k]);
   for (i=k=0; i<mh2; i++,k+=2)
      q[i] = -2.0 * cos(PI2 * f[k+1]);

   /* impulse response of analysis filter */
   xx = 1.0;
   xf = xff = 0.0;
   for (k=0; k<=m; k++) {
      if (flag_odd) {
         a0[0] = xx;
         b0[0] = xx - xff;
         xff = xf;
         xf  = xx;
      }
      else {
         a0[0] = xx + xf;
         b0[0] = xx - xf;
         xf = xx;
      }

      for (i=0; i<mh1; i++) {
         a0[i+1] = a0[i] + p[i] * a1[i] + a2[i];
         a2[i] = a1[i];
         a1[i] = a0[i];
      }
      for (i=0; i<mh2; i++) {
         b0[i+1] = b0[i] + q[i] * b1[i] + b2[i];
         b2[i] = b1[i];
         b1[i] = b0[i];
      }

      if (k!=0)
         a[k-1] = -0.5 * (a0[mh1] + b0[mh2]);

      xx = 0.0;
   }

   for (i=m-1; i>=0; i--)
      a[i+1] = -a[i];
   a[0] = 1.0;
   
   return;
}












#define	TRUE	1
#define	FALSE	0
//#define	PI	3.14159265
//#define PI2 6.2831853
#define MAXORD	100*2
#define N	128
#define NB	15
//#define EPSS	1.e-6

/**************************************************************************
*
* NAME
*	lsptopc 
*
* FUNCTION
*
*	convert lsp frequencies to predictor coefficients
*
* SYNOPSIS
*
*	subroutine lsptopc(f, pc, no)
*
*   formal
*			data	I/O
*	name		type	type	function
*	-------------------------------------------------------------------
*	f		real	i	lsp frequencies   [0...pi]
*	pc		real	o	LPC predictor coefficients
*   	no      	int     i   	number of lsp's
*
***************************************************************************
*
* DESCRIPTION
*
*	LSPTOPC converts line spectral frequencies to LPC predictor
*	coefficients.
*
*	The analysis filter may be reconstructed:
*
*		A(z) = 1/2 [ P(z) + Q(z) ]
*
*	CELP's LPC predictor coefficient convention is:
*              p+1         -(i-1)
*       A(z) = SUM   a   z          where a  = +1.0
*              i=1    i                    1
*
***************************************************************************/
#define	MAXNO   200 

int lsptopc(double *f, double *pc, int no)
{
  int i, k, noh, lspflag;
  double p[MAXNO / 2], q[MAXNO / 2];
  double a[MAXNO / 2 + 1], a1[MAXNO / 2 + 1], a2[MAXNO / 2 + 1];
  double b[MAXNO / 2 + 1], b1[MAXNO / 2 + 1], b2[MAXNO / 2 + 1];
  double pi, xx, xf;

  pi = 3.1415926535897931032;
  /* *check input for ill-conditioned cases 			 */
  lspflag = FALSE;
  if (f[0] <= 0.0 || f[no-1] >= pi)
  {
    printf("lsptopc: LSPs out of bounds; f(0) = %f (correction: interpolation)\n", f[0]);
    if (f[0] <= 0.0) f[0] = f[1]/2;
    if (f[no-1] >= pi) f[no-1] = f[no-1] + 0.5*(pi-f[no-1]);
	lspflag = TRUE;
    /* mexErrMsgTxt("lsptopc: LSPs out of bounds;"); */
  }
  for (i = 1; i < no; i++)
  {
    if (f[i] <= f[i - 1])
      lspflag = TRUE;
    if (f[i] <= 0.0 || f[i] >= pi)
    {
        printf("lsptopc: LSPs out of bounds; f(%d) = %f (correction: add perturbation)\n", i,f[i]);
        if (f[i] <= 0.0) f[i] = 0.001;
        if (f[i] >= pi) f[i] = pi-0.001;
	/* mexErrMsgTxt("lsptopc:  LSPs out of bounds"); */
    }
  }
  if (lspflag)
  {
        printf("LSFs = [ ");
        for(i=0;i<no;i++) { printf("%f\t",f[i]); f[i] = ((float)(pi*i)/(float)(no+1)); };
        printf(" ]\n");
        printf("lsptopc: nonmonotonic LSPs (correction: assume flat envelope)\n");
	/* mexErrMsgTxt("lsptopc: nonmonotonic LSPs"); */
  }


  /* *initialization 						 */

  noh = no / 2;
  for (i = 0; i < noh + 1; i++)
  {
    a[i] = 0.;
    a1[i] = 0.;
    a2[i] = 0.;
    b[i] = 0.;
    b1[i] = 0.;
    b2[i] = 0.;
  }

  /* *lsp filter parameters 					 */

  for (i = 0; i < noh; i++)
  {
    p[i] = -2. * cos(f[2 * i]);
    q[i] = -2. * cos(f[2 * i + 1]);
  }

  /* *impulse response of analysis filter 			 */

  xf = 0.0;
  for (k = 0; k < no + 1; k++)
  {
    xx = 0.0;
    if (k == 0)
      xx = 1.0;
    a[0] = xx + xf;
    b[0] = xx - xf;
    xf = xx;
    for (i = 0; i < noh; i++)
    {
      a[i + 1] = a[i] + p[i] * a1[i] + a2[i];
      b[i + 1] = b[i] + q[i] * b1[i] + b2[i];
      a2[i] = a1[i];
      a1[i] = a[i];
      b2[i] = b1[i];
      b1[i] = b[i];
    }
    if (k != 0)
      pc[k - 1] = -.5 * (a[noh] + b[noh]);
  }

  /* *convert to CELP's predictor coefficient array configuration */

  for (i = no - 1; i >= 0; i--)
    pc[i + 1] = -pc[i];
  pc[0] = 1.0;
  
  return lspflag;
}





/**************************************************************************
*
* NAME	
*	pctolsp
*
* FUNCTION
*
*	Compute LSP from predictor polynomial.
*
* SYNOPSIS
*
*	subroutine pctolsp(a,m,freq,lspflag)
*
*   formal 
*                       data	I/O
*	name		type	type	function
*	-------------------------------------------------------------------
*	a		double	i	a-polynomial a(0)=1
*	m		int	i	order of a
*	freq		double	o	lsp frequencies
*	N		int	na	grid points in search of zeros
*					of p-polynomials
*	EPS		double	na	precision for computing zeros
*	NB		int	na	iteration limit?
*
*   returns     lspflag int ill-conditioned lsp test
***************************************************************************
*	
* DESCRIPTION
*
*  	Compute lsp frequencies by disection method as described in:
*	
*	Line Spectrum Pair (LSP) and Speech Data Compression,
*	F.K. Soong and B-H Juang,
*       Proc. ICASSP 84, pp. 1.10.1-1.10.4
*
*	CELP's LPC predictor coefficient convention is:
*              p+1         -(i-1)
*       A(z) = SUM   a   z          where a  = +1.0
*              i=1    i                    1
*
*	Peter uses n=128, eps=1.e-06, nb=15 (this appears to be overkill!)
*
***************************************************************************
*
* CALLED BY
*
*	celp
*
* CALLS
*
*
**************************************************************************/
#define MAXORD	100*2
#define N	128
#define NB	15
#define EPS_2	1.e-10
#define FALSE	0
#define TRUE	1

int pctolsp(double *a, int m, double *freq)
{
  static double lastfreq[MAXORD];
  double tfreq[MAXORD];
  double p[MAXORD], q[MAXORD], ang, fm, tempfreq;
  double fr, pxr, tpxr, tfr, pxm, pxl, fl, qxl, tqxr;
  double qxm, qxr, tqxl;
  int mp, mh, nf, mb, jc, i, j;
  int lspflag;
 
  mp = m + 1;
  mh = m / 2;

  for (i = 0; i < mh; i++)
  {
    p[i] = a[i+1] + a[m-i];
    q[i] = a[i+1] - a[m-i];
  }

  fl = 0.;
  for (pxl = 1.0, j = 0; j < mh; j++)
    pxl += p[j];

  nf = 0;
  for (i = 1; i <= N; pxl = tpxr, fl = tfr, i++)
  {
    mb = 0;
    fr = i * (0.5 / N);
    pxr = cos(mp * PI * fr);
    for (j = 0; j < mh; j++)
    {
      jc = mp - (j+1)*2;
      ang = jc * PI * fr;
      pxr += cos(ang) * p[j];
    }
    tpxr = pxr;
    tfr = fr;
    if (pxl * pxr > 0.0) continue;

    do
    {
      mb++;
      fm = fl + (fr-fl) / (pxl-pxr) * pxl;
      pxm = cos(mp * PI * fm);
    
      for (j = 0; j < mh; j++)
      {
        jc = mp - (j+1) * 2;
        ang = jc * PI * fm;
        pxm += cos(ang) * p[j];
      }
      (pxm*pxl > 0.0) ? (pxl = pxm, fl = fm) : (pxr = pxm, fr = fm);

    } while ((fabs(pxm) > EPS_2) && (mb < 4));

    if ((pxl-pxr) * pxl == 0) 
    {
      for (j = 0; j < m; j++)
        tfreq[j] = (j+1) * 0.04545;
      printf("pctolsp: default LSPs used, avoiding /0\n");
      goto bye_bye;
    }
    tfreq[nf] = fl + (fr-fl) / (pxl-pxr) * pxl;
    nf += 2;
    if (nf > m-2) break;
  }

  tfreq[m] = 0.5;
  fl = tfreq[0];
  qxl = sin(mp * PI * fl);
  for (j = 0; j < mh; j++)
  {
    jc = mp - (j+1) * 2;
    ang = jc * PI * fl;
    qxl += sin(ang) * q[j];
  }

  for (i = 2; i < mp; qxl = tqxr, fl = tfr, i += 2)
  {
    mb = 0;
    fr = tfreq[i];
    qxr = sin(mp * PI * fr);
    for (j = 0; j < mh; j++)
    {
      jc = mp - (j+1) * 2;
      ang = jc * PI * fr;
      qxr += sin(ang) * q[j];
    }
    tqxl = qxl;
    tfr = fr;
    tqxr = qxr;
    
    do
    {
      mb++;
      fm = (fl+fr) * 0.5;
      qxm = sin(mp * PI * fm);

      for (j = 0; j < mh; j++)
      {
        jc = mp - (j+1) * 2;
        ang = jc * PI * fm;
        qxm += sin(ang) * q[j];
      }
      (qxm*qxl > 0.0) ? (qxl = qxm, fl = fm) : (qxr = qxm, fr = fm);

    } while ((fabs(qxm) > EPS_2*tqxl) && (mb < NB));

    if ((qxl-qxr) * qxl == 0)
    {
      for (j = 0; j < m; j++)
        tfreq[j] = lastfreq[j];
      printf("pctolsp: last lsps used, avoiding /0\n");
      goto bye_bye;
    }
    tfreq[i-1] = fl + (fr-fl) / (qxl-qxr) * qxl;
  }

  lspflag = FALSE;
  if (tfreq[0] == 0.0 || tfreq[0] == 0.5) 
    lspflag = TRUE;
  for (i = 1; i < m; i++)
  {
    if (tfreq[i] == 0.0 || tfreq[i] == 0.5) 
      lspflag = TRUE;


    if (tfreq[i]  <  tfreq[i-1]) 
    {
      lspflag = TRUE;
      printf("pctolsp: non-monotonic lsps\n");
      tempfreq = tfreq[i];
      tfreq[i] = tfreq[i-1];
      tfreq[i-1] = tempfreq;
    }
  }


  for (i = 1; i < m; i++)
  {
    if (tfreq[i]  <  tfreq[i-1])
    {
      printf("pctolsp: reset to previous lsp values\n");
      for (j = 0; j < m; j++)
        tfreq[j] = lastfreq[j];
      break;
    }
  }
  for (i = 0; i < m; i++) 
    lastfreq[i] = tfreq[i];

bye_bye:
  for (i = 0; i < m; i++) 
        freq[i] = tfreq[i]*PI2;          

  return lspflag;
}
