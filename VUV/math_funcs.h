#include <math.h>
#include <stdlib.h>
#include <stdio.h>

double *fliplr(double *arr,int l);
double **toeplitz(double *ar1, double *ar2, int s_ar1,int s_ar2);
double **transpose(double **A,int M, int N);
double **flip2d(int rows, int cols, double **arr);
double *median(int rows,int cols, double **arr);
double *filtfilt(double *b,double *a,double *x,int lb,int la,int lx);
double *medfilt(double *x,int p, int l);
void ellip(int rn,double rp,double rs,double wp,int knd,int fre,double *aHigh,double *bHigh);
