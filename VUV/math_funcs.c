#include "math_funcs.h"

double *fliplr(double *arr, int l){
    double *ar;
    int i;
    
    ar = (double*)malloc(sizeof(double)*(l));
    
    for (i = 0; i < l; i++){
        ar[i] = arr[(l-1)-i];
    }
    
    return ar;
}

double **flip2d(int rows,int cols,double **arr){
    int i,j;
    double **f_arr = calloc(rows, sizeof(double*));
    
    for(i = 0; i < rows; i++){
        f_arr[i] = calloc(cols, sizeof(double));
    }
    
    for(i = 0; i < rows; i++){
        for( j = 0; j < cols; j++){
            f_arr[i][j] = arr[i][(cols-1)-j];
        }
    }
    
    return f_arr;
}

/* Function : meanVal
 *
 * Purpose : calculates the mean value of the input array
 *
 * Input Arguments : double *s <- the number array
 * 		     int N     <- the length of the array
 *
 * Output Arguments: double <- the mean value of the input array
 *
 */
double meanVal(double *s, int N)
{
	double res;

	if (s == 0)
	{
		printf("meanVal() : Input argument is NULL!\n");
		exit(1);
	}

	if (N <= 0)
	{
		printf("meanVal() : N should be positive!\n");
		exit(1);
	}

	if(N % 2 == 0){
        res = (double)(s[N/2-1]+s[N/2])/ 2;
    }
    else{
        res = s[N/2];
    }
	return res;
}

/* Function : sort
 *
 * Purpose : sorts a real array in ascending order
 *
 * Input Arguments : int *array <- the array to be sorted
 *                   int N         <- the size of the array
 *
 * Output Arguments: -
 */
void sort(double *array, int N)
{ 
	int i, j; 
	double tmp;
    
	for (i = 0; i < N-1; i++) 
	{
		for (j = N-1; j > i; j--) 
		{
			if (array[j-1] > array[j]) 
			{
				tmp = array[j];
				array[j] = array[j-1];
				array[j-1] = tmp;
			} 
		}
	}
    
}

double *median(int rows, int cols, double **arr){
    double *temp;
    int i,j;
    double *y;
    
    temp = (double*)malloc(sizeof(double)*rows);
    y = (double*)malloc(sizeof(double)*cols);
    
    for(i = 0; i < cols; i++){
        for(j = 0; j < rows; j++){
            //printf("to arr einai : %d to i einai:%d kai to j einai: %d\n", arr[i][j],i,j);
            temp[j] = arr[j][i];
        }
        sort(temp,rows);
        y[i] = meanVal(temp,rows);
    }
    
    free(temp);
    return y;
}

double **toeplitz(double *ar1,double *ar2,int s_ar1, int s_ar2){
        int i,j;
        
        double *values = calloc(s_ar1*s_ar2, sizeof(double));
        double **res = malloc(s_ar1*sizeof(double*));
        for(i = 0; i < s_ar1; i++){
            res[i] = values + i*s_ar2;
        }
    
        /*
        double **res =(double*)malloc(s_ar1*sizeof(double*));
        for(i = 0; i < s_ar1; i++){
            res[i] = malloc(s_ar2*sizeof(double*));
        }
        */
        
        for(i = 0; i < s_ar1; i++){
            for(j = 0; j < s_ar2; j++){
                //printf("====To i einai: %d kai to j einai: %d====\n",i,j);
                //printf( " --> to ar1 einai :%lf kai to ar2 einai: %lf\n", ar1[i],ar2[j]);
                if( i == j ){
                    res[i][j] = ar1[0];
                }
                if(i > j){
                    res[i][j] = ar1[i-j];
                }
                if(j > i){
                    res[i][j] = ar2[j-i];
                }
                //printf("Evala mesa: %lf\n", res[i][j]);
            }
        }
        return res;
}


/* Function : transpose
 *
 * Purpose : calculates the transpose matrix of an input 2D matrix
 *
 * Input Arguments : double **A <- the 2D input matrix
 * 		     int M      <- the number of rows of the 2D matrix
 * 		     int N      <- the number of cols of the 2D matrix
 *
 * Output Arguments: double ** <- the transpose matrix
 */
double **transpose(double **A, int M, int N)
{
	int i, j;
	double **A_tr = calloc(N, sizeof(double *));

	if (A_tr == 0)
	{
		printf("Cannot allocate memory!\n");
		exit(1);
	}
    
	for(i = 0; i < N; i++)
	{
		if ((A_tr[i] = calloc(M, sizeof(double))) == 0)
		{
			printf("Cannot allocate memory!\n");
			exit(1);
		}	
	}
	
	for(i = 0; i < M; i++)
		for(j = 0; j < N; j++)
       			A_tr[j][i] = A[i][j];

	return A_tr;
}

/*fills a vector with 1 elements for a given distance */
int *ones(int *ar,int dis,int str){
    int i;
    
    for(i = 0; i < dis; i++){
        ar[i+str] = 1;
    }
    
    return ar;
}

double *d_ones(double *ar,int dis,int str){
    int i;
    
    for(i = 0; i < dis; i++){
        ar[i+str] = 1;
    }
    
    return ar;
}

double *m_ones(double *ar,int dis,int str){
    int i;
    
    for(i = 0; i < dis; i++){
        ar[i+str] = -1;
    }
    
    return ar;
}

//Calculates max element of an array

int maxEl(int *a, int N)
{
	int i;
	int max = a[0];

	if (N <= 0)
	{
		printf("maxEl() : N should be positive!\n");
		exit(1);
	}

	for (i = 0; i < N; i++)
		if (max < a[i])
			max = a[i];
	return max;
}

double **sparse(int *rows,int *cols, double *vals, int len){
    int i,j,pos_s,tr,tc,mx_r,mx_c;
    int *pos, *tmp_rows;
    double **spr;
    double tval;
    
    tmp_rows = malloc(sizeof(int)*len);
    tmp_rows = rows;
    
    pos = malloc(sizeof(int));
    pos_s = 0;
    
    for(i = 0; i < len; i++){
        if(vals[i] != 0){
            pos_s++;
            pos = realloc(pos,sizeof(int));
            pos[i] = i;
        }
    }
    
    mx_r = maxEl(rows,len);
    mx_c = maxEl(cols,len);
    
    spr = calloc(mx_r,sizeof(double*));
    for( i = 0; i < mx_r; i++){
        spr[i] = calloc(mx_c,sizeof(double));
    }
    
    for ( i = 0; i < pos_s; i++){
        
        if( tmp_rows[pos[i]] != 0){
            tr = tmp_rows[pos[i]];
            tc = cols[pos[i]];
            tval = vals[pos[i]];
        }
        for( j = i+1; j  < pos_s; j++){
            if(tr == tmp_rows[pos[j]] && tc == cols[pos[j]]) {
                tval += vals[pos[j]];
                tmp_rows[pos[j]] = 0;
            } 
        }
        
        spr[tr-1][tc-1] = tval;
        
    }
    
    return spr;
    
}

/* Function : ludcmp
 *
 * Purpose : given a matrix a[1...n][1..n], this routine replaces it by the LU
 *          decomposition of a rowwise permutation of itself. a and n are input.
 *          a is output, arranged appropriately. indx[1...n] is an output vector
 *          that records the row permutation effected by the partial pivoting.
 *          d is output as +-1 depending on whether the number of row interchanges
 *          was even or odd, respectively. This routine is used in combination with
 *          lubksb to solve linear equations or invert a matrix
 *
 * Input Arguments : double **a <- the matrix A
 *                   int n      <- the size of matrix A (nxn)
 *
 * (Output Arguments: )
 *                   int *indx <- petrmutation vector
 * 		     double *d <- 1 for even row interchanges
 * 		                  -1 for odd interchanges
 *
 * Taken from : Numerical Recipes in C (1992)
 */
void ludcmp(double **a, int n, int *indx, double *d)
{
	int i, imax, j, k;
	double big, dum, sum, temp;
	double *vv;

	if ((vv = calloc(n, sizeof(double))) == 0)
	{
		printf("Cannot allocate memory!\n");
		exit(1);
	}
	vv = vv - 1;

	*d = 1.0;

	for (i = 1; i <= n; i++)
	{
		big = 0.0;

		for (j = 1; j <= n; j++)
			if ((temp = fabs(a[i][j])) > big)
				big = temp;

		if (big == 0.0)
		{
			printf("Singular matrix in routine ludcmp");
			exit(1);
		}

		vv[i] = 1.0/big;
	}

	for (j = 1; j <= n; j++)
	{
		for (i = 1; i < j; i++)
		{
			sum = a[i][j];

			for (k = 1; k < i; k++)
				sum -= a[i][k]*a[k][j];

			a[i][j] = sum;
		}

		big = 0.0;

		for (i = j; i <= n; i++)
		{
			sum = a[i][j];

			for (k = 1; k < j; k++)
				sum -= a[i][k] * a[k][j];

			a[i][j] = sum;

			if ((dum = vv[i]*fabs(sum)) >= big)
			{
				big = dum;
				imax = i;
			}
		}

		if (j != imax)
		{
			for (k = 1; k <= n; k++)
			{
				dum = a[imax][k];
				a[imax][k] = a[j][k];
				a[j][k] = dum;
			}

			*d = -(*d);
			vv[imax] = vv[j];
		}

		indx[j] = imax;

		if (a[j][j] == 0.0)
			a[j][j] = 1.0e-20;

		if (j != n)
		{
			dum = 1.0/(a[j][j]);

			for (i = j + 1; i <= n; i++)
				a[i][j] *= dum;
		}
	}

	vv = vv + 1;
	free(vv);
}

/* Function : lubksb
 *
 * Purpose : solves the set of linear equations AX = B. Here, a[1...n][1...n]
 *          input, not as the matrix A but rahter as its LU decomposition,
 *          determined by the routine ludcmp. indx[1..n] is input as the
 *          permutation vector returned by ludcmp. b[1...n] is input as the
 *          right hand side vector B, and returns with the solution vector X.
 *          a, n, and indx are not modified by this routine and can be left in
 *          place for successive calls with different right-hand sides b. This
 *          routine takes into account the possibility that b will begin with
 *          many zero elements, so it is efficient for use in matrix inversion
 *
 * Input Arguments : double **a <- the matrix A
 *                   int n      <- the size of matrix A (nxn)
 *                   int *indx  <- petrmutation vector
 * (Output Arguments: )
 * 		     double *b <- the vector B (input)
 * 		                  the solution of the system AX=B (output)
 *
 * Taken from : Numerical Recipes in C (1992)
 */
void lubksb(double **a, int n, int *indx, double *b)
{
	int i, ii = 0, ip, j;
	double sum;

	for (i = 1; i <= n ; i++)
	{
		ip = indx[i];
		sum = b[ip];
		b[ip]=b[i];
		
		if (ii)
			for (j = ii; j <= i-1; j++)
				sum -= a[i][j] * b[j];
		else if (sum)
			ii = i;
		
		b[i] = sum;
	}
	for (i = n; i >= 1; i--)
	{
		sum=b[i];

		for (j = i+1; j <= n; j++)
			sum -= a[i][j] * b[j];

		b[i] = sum/a[i][i];
	}
}

/* Function : LUdecomp
 *
 * Purpose : LU decomposition for solution of a linear system of equations AX = B
 *
 * Input Arguments : double **a <- the matrix A
 *                  int n      <- the size of matrix A (nxn)
 *
 * (Output Arguments: )
 * 		    double *b <- the matrix B (as input)
 * 		                 the solution of the system AX = B (as output)
 */
void LUdecomp(double **a, int n, double *b)
{
	int i, j;
	double d;
	int *indx;

	if ((indx = calloc(n, sizeof(int))) == 0)
	{
		printf("Cannot allocate memory!\n");
		exit(1);
	}

	for (i = 0; i < n; i++)
		*(a + i) = *(a + i) - 1;

	a = a - 1;
	b = b - 1;
	indx = indx - 1;

	ludcmp(a, n, indx, &d);

	lubksb(a, n, indx, b);


	a = a + 1;
	for (i = 0; i < n; i++)
		*(a + i) = *(a + i) + 1;

	b = b + 1;
	indx = indx + 1;
    

	free(indx);
}

/* Function : my_IIR_filter
 *
 * Purpose : filters the input array with an IIR filter described by the input arguments
 *
 * Input Arguments : double *B <- the filter nominator array
 * 		     double *A <- the filter denominator array
 * 		     int M     <- the length of the filter nominator array
 *    		     int N     <- the length of the filter denominator array
 * 		     double *X <- the input array
 * 		     int K     <- the length of the input array
 *
 * (Output Arguments:) 
 * 		     double *Y <- the result of the FIR filtering,
 *                                it is supposed to be preallocated
 *
 */
void my_IIR_filter(double *B, double *A, int M, int N, double *X, int K, double *Y)
{
	int i, j;
	double FFV, FBV, tmp;

	if (B == 0 || A == 0)
	{
		printf("my_IIR_filter() : Filter coefficients are NULL!\n");
		exit(1);
	}
	if (X == 0 || Y == 0)
	{
		printf("my_IIR_filter() : Input or Output arguments are NULL!\n");
		exit(1);
	}
	if (N <= 0 || M <= 0 || K <= 0)
	{
		printf("my_IIR_filter() : M, N, K must be positive!\n");
		exit(1);
	}

	if (A[0] != 1.0)
	{
		tmp = A[0];
		for (i = 0; i < N; i++)
		{
			A[i] = A[i]/tmp;
			//printf("A[%d] = %lf\n", i, A[i]);
		}
		for (i = 0; i < M; i++)
		{
			B[i] = B[i]/tmp;
			//printf("B[%d] = %lf\n", i, B[i]);
		}
	}

	for (i = 0; i < K; i++)
	{
		FFV = 0.0;
		FBV = 0.0;

		for (j = 1; j < N; j++)
		{
			if (i - j < 0)
				break;
			else
				FBV += (A[j] * Y[i-j]);
		}

		for (j = 0; j < M; j++)
		{
			if (i - j < 0)
				break;
			else
				FFV += (B[j] * X[i-j]);
		}
		
		Y[i] = FFV - FBV;
	}
}


double *filtfilt(double *b, double *a,double *x,int lb, int la,int lx){
    int i;
    double *y1,*y2;
    int ly = lx;
    y1 = malloc(sizeof(double)*lx);
    y2 = malloc(sizeof(double)*lx);

    my_IIR_filter(b,a,lb,la,x,lx,y1);
    for( i = 0; i < ly; i++){
        printf("%lf  ",y1[i]);
    }
    printf("\n\n");
    
    y1 = fliplr(y1,ly);
    for( i = 0; i < ly; i++){
        printf("%lf  ",y1[i]);
    }
    printf("\n\n");
    
    my_IIR_filter(b,a,lb,la,y1,ly,y2);
    for( i = 0; i < ly; i++){
        printf("%lf  ",y2[i]);
    }
    printf("\n\n");
    
    y2 = fliplr(y2,ly);
    for( i = 0; i < ly; i++){
        printf("%lf  ",y2[i]);
    }
    printf("\n\n");
    
    return y2;
     
}

/*void ellip(int n,double rp,double rs,double wp){
    double u;
    int fs = 2;
    
    printf("to wp einai:%lf \n",wp);
    u = 2*fs*atan(M_PI*wp/fs);
    printf("%lf  \n",u);
    
}*/

double *medfilt(double *x, int p, int l){
    double *y;
    double *tempx,*tempx1,*tempx2;
    double **res,**res_tr;
    
    int i,j;
    
    int ad = (p-1)/2;
    
    if(ad == 0){
        y = x;
        return y;
    }
    printf("to size einai: %d\n",l); //GAITI TO THELW GIA NA MH GINEI ALLAGI STO NOUMER?
    tempx = (double*)malloc(sizeof(double)*(ad+l+ad));
    
    for(i = 0; i < ad; i++){
        tempx[i] = x[0] * 1;
    }
    
    for( i = 0; i < l; i++){
        tempx[i+ad] = x[i];
    }
    
    for(i = 0; i < ad; i++){
        tempx[i+ad+l] = x[l-1] * 1;
    }
    
    for(i = 0; i < ad+l+ad; i++){
       //printf(" mpika %lf \n", tempx[i]);
    }
    
    
    tempx1 = fliplr(tempx,l);
    
    /*for(i = 0; i < l; i++){
        printf("sto x1 exw: %lf\n",tempx1[i]);
    }*/
    
    tempx2 = (double*)malloc(sizeof(double)*(p));
    
    for(i = 0; i < p; i++){
        tempx2[i] = tempx[(l-1)+i];
    }
    
    /*for(i = 0; i < p; i++){
        printf("sto x2 exw: %lf\n",tempx2[i]);
    }*/
    
    res = toeplitz(tempx1,tempx2,l,p);
    /*for(i = 0; i < l; i++){
        for( j = 0 ; j < p; j++){
            printf("%lf  ",res[i][j]);
        }
        printf("\n");
        printf("\n");
    }
    
    printf("\n");
    printf("\n");
    */
    
    res_tr = transpose(res,l,p);
    /*for(i = 0; i < p; i++){
        for( j = 0 ; j < l; j++){
            printf("%lf  ",res_tr[i][j]);
        }
        printf("\n");
    }
    printf("\n");
    printf("\n");
    */
    res_tr = flip2d(p,l,res_tr);
    
    /*printf("\n");
    printf("\n");
    for(i = 0; i < p; i++){
        for( j = 0 ; j < l; j++){
            printf("%lf  ",res_tr[i][j]);
        }
        printf("\n");
    }
    
    //printf("\n");
    //printf("\n");
    */
    y = median(p,l,res_tr);
    
    return y;
}


 
 
